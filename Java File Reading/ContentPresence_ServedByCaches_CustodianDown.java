import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ContentPresence {

	public static int getContentPresentValues(FileInputStream fstream) throws Exception{

		int count=1;
		int num=0;
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String strLine;
		try{
			while ((strLine = br.readLine())!=null){
				//System.out.println("counting line: "+count);
				int i= Integer.parseInt(strLine);
				System.out.print("\nentry is: "+i);
				System.out.print("       num is: "+num);
				int d=i-num;
				System.out.print("     difference is:"+d);
				num=i;
				count++;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("line count: "+count);
		return count;
	}


	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		FileInputStream fstream = new FileInputStream("C:/Users/Anubhab/Desktop/anand/lcd_content_presence_alpha_02_cache_8000.txt");
		System.out.println(getContentPresentValues(fstream));
	}

}
