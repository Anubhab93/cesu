import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ErrorbarPlotting {
	
	public static ArrayList<Integer> nodeValues(FileInputStream fstream) throws Exception{
		
		ArrayList<Integer> nodes= new ArrayList<Integer>();
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String strLine;
		try{
			while ((strLine = br.readLine()) != null){
				nodes.add(Integer.parseInt(strLine));
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return nodes;
	}
	
	public static int servedByNetwork(ArrayList<Integer> nodes) throws Exception{
		
		int count=0,netwk=0;
		while(count<30000){
			if(nodes.get(count)<1000){
				netwk++;
			}
			count++;
		}
		return (netwk*100)/30000;
	}
	
	public static ArrayList<Integer> findMaxMin(ArrayList<Integer> results) throws Exception{
		
		ArrayList<Integer> result= new ArrayList<Integer>();
		int max=0,min=1000,i=0;
		while(i<results.size()){
			int r=results.get(i);
			if(max<r){
				max=r;
			}
			if(r<min){
				min=r;
			}
			i++;
		}
		result.add(max);
		result.add(min);
		return result;
	}
	
	public static void main(String []args) throws Exception{
		
		FileInputStream fstream = new FileInputStream("E:/FFOutput/icarus-0.5.0/Results/served requests/8CL4M06.txt");
		ArrayList<Integer> nodes= new ArrayList<Integer>();
		nodes=nodeValues(fstream);   //size=4,50,000..records all serving nodes
		ArrayList<Integer> NetworkResults= new ArrayList<Integer>();  //records percentage of request served by each expt, of size 15
		int count=0;
		while(count<450000){
			int i=count;
			System.out.println("count starts at: "+i);
			ArrayList<Integer> results= new ArrayList<Integer>();  //records serving nodes of every expt, i.e. every block of 30,000
			while(i<(count+30000)){
				results.add(nodes.get(i));
				i++;
			}
			NetworkResults.add(servedByNetwork(results));
			count=count+30000;
		}
		System.out.println("network results are: "+NetworkResults);
		System.out.println("maximum is: "+findMaxMin(NetworkResults).get(0)+" Minimum is: "+findMaxMin(NetworkResults).get(1));
	}

}
