import numpy as np
import matplotlib.pyplot as plt

N=4

pot=(18.2, 20.4, 21.2, 20.5)
potStd=(0.2, 0.2, 0.1, 1.1)
served=(10.3, 13.1, 14.3, 14.6)
servedStd=(0.6, 0.7, 0.5, 0.8)
myalgo=(17.9, 19.4, 19.8, 19.2)
myalgoStd=(0.5, 0.6, 0.4, 0.9)

ind = np.arange(N)
width = 0.25

fig, ax = plt.subplots()

rects1 = ax.bar(ind, served, width, color='b', yerr=servedStd, ecolor='k')
rects2 = ax.bar(ind+width, myalgo, width, color='g', yerr=myalgoStd, ecolor='k')
rects3 = ax.bar(ind+width+width, pot, width, color='r', yerr=potStd, ecolor='k')

ax.set_ylabel('Percentage of Served Requests')
ax.set_xlabel('Content Population (in multiples of 10^3)')
ax.set_title('Performance of Proposed Algorithm')
ax.set_xticks(ind + width+0.15)
ax.set_xticklabels(('10', '20', '50', '100'))

ax.legend((rects1[0], rects2[0], rects3[0]), ('requests served by shortest path', 'Requests Served by Proposed Algorithm','Request Serving Potential'))

def autolabel(rects):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        """ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')"""

autolabel(rects1)
autolabel(rects2)
autolabel(rects3)

plt.show()
