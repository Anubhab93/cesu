import numpy as np
import matplotlib.pyplot as plt

N=4

pot_gt=(12.4, 21.3, 60, 73.3)
potStd_gt=(0.9, 1.2, 1.5, 1.4)
served_gt=(7.6, 13.9, 51.3, 65.1)
servedStd_gt=(0.9, 1.8, 1.1, 1.7)
myalgo_gt=(11.5, 19.3, 58.2, 72.1)
myalgoStd_gt=(0.9, 1.4, 1.5, 1.9)

pot_gr=(15.6, 33.9, 60.3, 73.3)
potStd_gr=(0.8, 1.0, 0.9, 0.9)
served_gr=(6.2, 18.1, 45.1, 58.1)
servedStd_gr=(1.0, 0.7, 0.8, 1.0)
myalgo_gr=(14.1, 33.1, 59.5, 72.6)
myalgoStd_gr=(0.2, 0.4, 0.3, 0.4)

pot_wd=(7.7, 22.3, 42, 53.3)
potStd_wd=(0.9, 1, 0.9, 1.2)
served_wd=(4.5, 16.1, 33.3, 45.1)
servedStd_wd=(1.2, 0.8, 1.1, 1.2)
myalgo_wd=(6.8, 20.6, 41.2, 52.9)
myalgoStd_wd=(0.9, 1.4, 1.5, 1.2)

ind = np.arange(N)
width = 0.07

fig, ax = plt.subplots()

rects1 = ax.bar(ind, served_gt, width, color='g', yerr=servedStd_gt, ecolor='k')
rects2 = ax.bar(ind+width, myalgo_gt, width, color='lawngreen', yerr=myalgoStd_gt, ecolor='k')
rects3 = ax.bar(ind+width+width, pot_gt, width, color='lightgreen', yerr=potStd_gt, ecolor='k')
rects4 = ax.bar(ind+width+width+width+width, served_gr, width, color='r', yerr=servedStd_gr, ecolor='k')
rects5 = ax.bar(ind+width+width+width+width+width, myalgo_gr, width, color='tomato', yerr=myalgoStd_gr, ecolor='k')
rects6 = ax.bar(ind+width+width+width+width+width+width, pot_gr, width, color='lightsalmon', yerr=potStd_gr, ecolor='k')
rects7 = ax.bar(ind+width+width+width+width+width+width+width+width, served_wd, width, color='darkcyan', yerr=servedStd_wd, ecolor='k')
rects8 = ax.bar(ind+width+width+width+width+width+width+width+width+width, myalgo_wd, width, color='navy', yerr=myalgoStd_wd, ecolor='k')
rects9 = ax.bar(ind+width+width+width+width+width+width+width+width+width+width, pot_wd, width, color='blue', yerr=potStd_wd, ecolor='k')

ax.set_ylabel('Percentage of Served Requests')
ax.set_xlabel('Alpha')
ax.set_title('Performance of Proposed Algorithm')
ax.set_xticks(ind+width+width+width+width+width+width+width+width+width+width+0.10)
ax.set_xticklabels(('0.6', '0.8', '1.0', '1.1'))

ax.legend((rects1[0], rects2[0], rects3[0], rects4[0], rects5[0], rects6[0], rects7[0], rects8[0], rects9[0]),
          ('requests_served_shortest_path_GEANT', 'requests_served_proposed_algorithm_GEANT','request_serving_potential_GEANT',
           'requests_served_shortest_path_GARR', 'requests_served_proposed_algorithm_GARR','request_serving_potential_GARR',
           'requests_served_shortest_path_WIDE', 'requests_served_proposed_algorithm_WIDE','request_serving_potential_WIDE'))

def autolabel(rects):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        """ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')"""

autolabel(rects1)
autolabel(rects2)
autolabel(rects3)
autolabel(rects4)
autolabel(rects5)
autolabel(rects6)
autolabel(rects7)
autolabel(rects8)
autolabel(rects9)

plt.show()
