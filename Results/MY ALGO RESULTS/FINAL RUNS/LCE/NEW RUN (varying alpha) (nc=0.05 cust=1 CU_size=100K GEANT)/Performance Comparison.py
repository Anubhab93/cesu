import numpy as np
import matplotlib.pyplot as plt

N=4

pot=(12.4, 21.3, 60, 73.3)
potStd=(0.4, 0.2, 0.3, 0.2)
served=(7.6, 13.9, 51.3, 65.1)
servedStd=(0.6, 0.8, 1.1, 1.2)
myalgo=(11.5, 19.3, 58.2, 72.1)
myalgoStd=(0.3, 0.4, 0.5, 0.4)

ind = np.arange(N)
width = 0.25

fig, ax = plt.subplots()

rects1 = ax.bar(ind, served, width, color='b', yerr=servedStd, ecolor='k')
rects2 = ax.bar(ind+width, myalgo, width, color='g', yerr=myalgoStd, ecolor='k')
rects3 = ax.bar(ind+width+width, pot, width, color='r', yerr=potStd, ecolor='k')

ax.set_ylabel('Percentage of Served Requests')
ax.set_xlabel('Alpha')
ax.set_title('Performance of Proposed Algorithm')
ax.set_xticks(ind + width+0.15)
ax.set_xticklabels(('0.6', '0.8', '1.0', '1.1'))

ax.legend((rects1[0], rects2[0], rects3[0]), ('requests served by shortest path', 'Requests Served by Proposed Algorithm','Request Serving Potential'))

def autolabel(rects):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        """ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')"""

autolabel(rects1)
autolabel(rects2)
autolabel(rects3)

plt.show()
