import numpy as np
import matplotlib.pyplot as plt

N=3

pot_wd=(6.4, 8.8, 16.8)
potStd_wd=(0.6, 0.7, 1.5)
served_wd=(4.6, 5.5, 13)
servedStd_wd=(0.6, 0.5, 1.5)
myalgo_wd=(6.3, 8.8, 16.7)
myalgoStd_wd=(0.3, 0.2, 0.4)

pot_gr=(7.6, 9.5, 18)
potStd_gr=(0.8, 1.5, 2)
served_gr=(3.3, 5.4, 10.9)
servedStd_gr=(0.7, 0.6, 1.3)
myalgo_gr=(7.5, 9.4, 17.8)
myalgoStd_gr=(0.1, 0.2, 0.2)

pot_gt=(8.8, 11.6, 20.5)
potStd_gt=(0.2, 0.3, 1.0)
served_gt=(4.6, 6.6, 14.6)
servedStd_gt=(0.5, 0.9, 0.8)
myalgo_gt=(8.3, 11, 19.2)
myalgoStd_gt=(0.2, 0.5, 0.9)

ind = np.arange(N)
width = 0.07

fig, ax = plt.subplots()

rects1 = ax.bar(ind, served_gt, width, color='g', yerr=servedStd_gt, ecolor='k')
rects2 = ax.bar(ind+width, myalgo_gt, width, color='lawngreen', yerr=myalgoStd_gt, ecolor='k')
rects3 = ax.bar(ind+width+width, pot_gt, width, color='lightgreen', yerr=potStd_gt, ecolor='k')
rects4 = ax.bar(ind+width+width+width+width, served_gr, width, color='r', yerr=servedStd_gr, ecolor='k')
rects5 = ax.bar(ind+width+width+width+width+width, myalgo_gr, width, color='tomato', yerr=myalgoStd_gr, ecolor='k')
rects6 = ax.bar(ind+width+width+width+width+width+width, pot_gr, width, color='lightsalmon', yerr=potStd_gr, ecolor='k')
rects7 = ax.bar(ind+width+width+width+width+width+width+width+width, served_wd, width, color='darkcyan', yerr=servedStd_wd, ecolor='k')
rects8 = ax.bar(ind+width+width+width+width+width+width+width+width+width, myalgo_wd, width, color='navy', yerr=myalgoStd_wd, ecolor='k')
rects9 = ax.bar(ind+width+width+width+width+width+width+width+width+width+width, pot_wd, width, color='blue', yerr=potStd_wd, ecolor='k')

ax.set_ylabel('Percentage of Served Requests')
ax.set_xlabel('Network Cache')
ax.set_title('Performance of Proposed Algorithm')
ax.set_xticks(ind+width+width+width+width+width+width+width+width+width+width+0.10)
ax.set_xticklabels(('0.005', '0.01', '0.05'))
ax.set_ylim(0, 40)

ax.legend((rects1[0], rects2[0], rects3[0], rects4[0], rects5[0], rects6[0], rects7[0], rects8[0], rects9[0]),
          ('requests_served_shortest_path_GEANT', 'requests_served_proposed_algorithm_GEANT','request_serving_potential_GEANT',
           'requests_served_shortest_path_GARR', 'requests_served_proposed_algorithm_GARR','request_serving_potential_GARR',
           'requests_served_shortest_path_WIDE', 'requests_served_proposed_algorithm_WIDE','request_serving_potential_WIDE'))

def autolabel(rects):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        """ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')"""

autolabel(rects1)
autolabel(rects2)
autolabel(rects3)
autolabel(rects4)
autolabel(rects5)
autolabel(rects6)
autolabel(rects7)
autolabel(rects8)
autolabel(rects9)

plt.show()
