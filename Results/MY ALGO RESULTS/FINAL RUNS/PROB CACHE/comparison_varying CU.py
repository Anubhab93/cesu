import numpy as np
import matplotlib.pyplot as plt

N=3

pot_wd=(19.5, 20.2, 21.1)
potStd_wd=(1.2, 1.4, 1.3)
served_wd=(14.3, 15.1, 15.9)
servedStd_wd=(1.1, 1.5, 1.4)
myalgo_wd=(18.8, 20.1, 21)
myalgoStd_wd=(0.3, 0.4, 0.5)

pot_gr=(14.1, 17.3, 18.9)
potStd_gr=(1.8, 1.9, 1.6)
served_gr=(7.3, 10.3, 11.3)
servedStd_gr=(1.9, 2.1, 2.2)
myalgo_gr=(14, 17.2, 18.5)
myalgoStd_gr=(0.2, 0.3, 0.3)

pot_gt=(16.4, 17.8, 19.3)
potStd_gt=(2.3, 1.9, 2.5)
served_gt=(10.2, 11.7, 13.4)
servedStd_gt=(2.1, 1.9, 2.3)
myalgo_gt=(16.3, 17.7, 19.3)
myalgoStd_gt=(0.3, 0.4, 0.3)

ind = np.arange(N)
width = 0.07

fig, ax = plt.subplots()

rects1 = ax.bar(ind, served_gt, width, color='g', yerr=servedStd_gt, ecolor='k')
rects2 = ax.bar(ind+width, myalgo_gt, width, color='lawngreen', yerr=myalgoStd_gt, ecolor='k')
rects3 = ax.bar(ind+width+width, pot_gt, width, color='lightgreen', yerr=potStd_gt, ecolor='k')
rects4 = ax.bar(ind+width+width+width+width, served_gr, width, color='r', yerr=servedStd_gr, ecolor='k')
rects5 = ax.bar(ind+width+width+width+width+width, myalgo_gr, width, color='tomato', yerr=myalgoStd_gr, ecolor='k')
rects6 = ax.bar(ind+width+width+width+width+width+width, pot_gr, width, color='lightsalmon', yerr=potStd_gr, ecolor='k')
rects7 = ax.bar(ind+width+width+width+width+width+width+width+width, served_wd, width, color='darkcyan', yerr=servedStd_wd, ecolor='k')
rects8 = ax.bar(ind+width+width+width+width+width+width+width+width+width, myalgo_wd, width, color='navy', yerr=myalgoStd_wd, ecolor='k')
rects9 = ax.bar(ind+width+width+width+width+width+width+width+width+width+width, pot_wd, width, color='blue', yerr=potStd_wd, ecolor='k')

ax.set_ylabel('Percentage of Served Requests')
ax.set_xlabel('Content Population (in 10^3)')
ax.set_title('Performance of Proposed Algorithm')
ax.set_xticks(ind+width+width+width+width+width+width+width+width+width+width+0.10)
ax.set_xticklabels(('10', '20', '50'))
ax.set_ylim(0, 40)

ax.legend((rects1[0], rects2[0], rects3[0], rects4[0], rects5[0], rects6[0], rects7[0], rects8[0], rects9[0]),
          ('requests_served_shortest_path_GEANT', 'requests_served_proposed_algorithm_GEANT','request_serving_potential_GEANT',
           'requests_served_shortest_path_GARR', 'requests_served_proposed_algorithm_GARR','request_serving_potential_GARR',
           'requests_served_shortest_path_WIDE', 'requests_served_proposed_algorithm_WIDE','request_serving_potential_WIDE'))

def autolabel(rects):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        """ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')"""

autolabel(rects1)
autolabel(rects2)
autolabel(rects3)
autolabel(rects4)
autolabel(rects5)
autolabel(rects6)
autolabel(rects7)
autolabel(rects8)
autolabel(rects9)

plt.show()
