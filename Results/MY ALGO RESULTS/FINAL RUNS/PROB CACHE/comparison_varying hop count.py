import numpy as np
import matplotlib.pyplot as plt

x = [2, 5, 8]

gr=[13, 20, 21]

gt=[10, 14, 16]

wd=[9, 16, 20]

plt.plot(x, gt, 'g--', label='GEANT')
plt.plot(x, gr, 'r--', label='GARR')
plt.plot(x, wd, 'b--', label='WIDE')

plt.ylabel('No. of evaluated requests (in 10^6)')
plt.xlabel('Value of hop_count')
plt.axis([0, 10, 0, 30])

plt.legend()
plt.show()
