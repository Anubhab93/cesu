import numpy as np
import matplotlib.pyplot as plt

N=4

pot_gt=(20.2, 19.1, 13.5, 20.8)
potStd_gt=(1.2, 1.8, 0.5, 1.1)
served_gt=(13.1, 13.2, 6.5, 14.2)
servedStd_gt=(0.6, 1.1, 0.4, 0.5)
myalgo_gt=(19.5, 18.1, 12.5, 18.5)
myalgoStd_gt=(0.4, 0.5, 0.4, 0.4)

pot_gr=(16.6, 16.8, 11.2, 15.9)
potStd_gr=(1.8, 1.4, 1.5, 0.7)
served_gr=(8.4, 9.3, 4.9, 8.6)
servedStd_gr=(0.6, 0.9, 0.3, 0.8)
myalgo_gr=(16.2, 15.9, 10.3, 13.8)
myalgoStd_gr=(0.4, 0.3, 0.4, 0.5)

pot_wd=(14.6, 14.3, 6.9, 11)
potStd_wd=(1.3, 1.2, 1.1, 0.8)
served_wd=(10.2, 10.3, 5.1, 8.8)
servedStd_wd=(0.4, 0.8, 0.9, 0.7)
myalgo_wd=(14.2, 14, 6.2, 9.9)
myalgoStd_wd=(0.4, 0.3, 0.3, 0.4)

ind = np.arange(N)
width = 0.07

fig, ax = plt.subplots()

rects1 = ax.bar(ind, served_gt, width, color='g', yerr=servedStd_gt, ecolor='k')
rects2 = ax.bar(ind+width, myalgo_gt, width, color='lawngreen', yerr=myalgoStd_gt, ecolor='k')
rects3 = ax.bar(ind+width+width, pot_gt, width, color='lightgreen', yerr=potStd_gt, ecolor='k')
rects4 = ax.bar(ind+width+width+width+width, served_gr, width, color='r', yerr=servedStd_gr, ecolor='k')
rects5 = ax.bar(ind+width+width+width+width+width, myalgo_gr, width, color='tomato', yerr=myalgoStd_gr, ecolor='k')
rects6 = ax.bar(ind+width+width+width+width+width+width, pot_gr, width, color='lightsalmon', yerr=potStd_gr, ecolor='k')
rects7 = ax.bar(ind+width+width+width+width+width+width+width+width, served_wd, width, color='darkcyan', yerr=servedStd_wd, ecolor='k')
rects8 = ax.bar(ind+width+width+width+width+width+width+width+width+width, myalgo_wd, width, color='navy', yerr=myalgoStd_wd, ecolor='k')
rects9 = ax.bar(ind+width+width+width+width+width+width+width+width+width+width, pot_wd, width, color='blue', yerr=potStd_wd, ecolor='k')

ax.set_ylim([0, 90])
ax.set_ylabel('Percentage of Served Requests')
#ax.set_xlabel('A')
ax.set_title('Standard Parameters')
ax.set_xticks(ind+width+width+width+width+width+width)
ax.set_xticklabels(('CL4M', 'LCD', 'LCE', 'PROB CACHE'))

ax.legend((rects1[0], rects2[0], rects3[0], rects4[0], rects5[0], rects6[0], rects7[0], rects8[0], rects9[0]),
          ('requests_served_shortest_path_GEANT', 'requests_served_proposed_algorithm_GEANT','request_serving_potential_GEANT',
           'requests_served_shortest_path_GARR', 'requests_served_proposed_algorithm_GARR','request_serving_potential_GARR',
           'requests_served_shortest_path_WIDE', 'requests_served_proposed_algorithm_WIDE','request_serving_potential_WIDE'))

def autolabel(rects):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        """ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')"""

autolabel(rects1)
autolabel(rects2)
autolabel(rects3)
autolabel(rects4)
autolabel(rects5)
autolabel(rects6)
autolabel(rects7)
autolabel(rects8)
autolabel(rects9)

plt.show()
