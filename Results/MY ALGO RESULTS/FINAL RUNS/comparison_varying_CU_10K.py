import numpy as np
import matplotlib.pyplot as plt

N=4

pot_gt=(15.1, 14.3, 11.5, 12.5)
potStd_gt=(1.2, 1.1, 1.3, 1.1)
served_gt=(9.3, 9, 9.9, 9.1)
servedStd_gt=(0.8, 0.7, 1.4, 0.7)
myalgo_gt=(14.1, 13.2, 10.6, 11.4)
myalgoStd_gt=(0.6, 0.5, 0.6, 0.4)

pot_gr=(11.5, 21.2, 8.4, 11.9)
potStd_gr=(0.6, 0.7, 0.8, 0.4)
served_gr=(5.4, 11.1, 2.4, 5.9)
servedStd_gr=(0.7, 0.9, 0.9, 0.5)
myalgo_gr=(10.8, 19.9, 7.3, 10.6)
myalgoStd_gr=(0.5, 0.5, 0.8, 0.4)

pot_wd=(10.1, 19.5, 5.5, 7.2)
potStd_wd=(0.5, 0.6, 1.3, 1.1)
served_wd=(6.8, 14.3, 3.8, 6.1)
servedStd_wd=(0.3, 0.6, 1.3, 0.5)
myalgo_wd=(9.1, 18.9, 4.8, 6.6)
myalgoStd_wd=(0.4, 0.3, 0.4, 0.5)

ind = np.arange(N)
width = 0.07

fig, ax = plt.subplots()

rects1 = ax.bar(ind, served_gt, width, color='g', yerr=servedStd_gt, ecolor='k')
rects2 = ax.bar(ind+width, myalgo_gt, width, color='lawngreen', yerr=myalgoStd_gt, ecolor='k')
rects3 = ax.bar(ind+width+width, pot_gt, width, color='lightgreen', yerr=potStd_gt, ecolor='k')
rects4 = ax.bar(ind+width+width+width+width, served_gr, width, color='r', yerr=servedStd_gr, ecolor='k')
rects5 = ax.bar(ind+width+width+width+width+width, myalgo_gr, width, color='tomato', yerr=myalgoStd_gr, ecolor='k')
rects6 = ax.bar(ind+width+width+width+width+width+width, pot_gr, width, color='lightsalmon', yerr=potStd_gr, ecolor='k')
rects7 = ax.bar(ind+width+width+width+width+width+width+width+width, served_wd, width, color='darkcyan', yerr=servedStd_wd, ecolor='k')
rects8 = ax.bar(ind+width+width+width+width+width+width+width+width+width, myalgo_wd, width, color='navy', yerr=myalgoStd_wd, ecolor='k')
rects9 = ax.bar(ind+width+width+width+width+width+width+width+width+width+width, pot_wd, width, color='blue', yerr=potStd_wd, ecolor='k')

ax.set_ylabel('Percentage of Served Requests')
#ax.set_xlabel('A')
ax.set_title('Content Universe=10K')
ax.set_xticks(ind+width+width+width+width+width+width)
ax.set_xticklabels(('CL4M', 'LCD', 'LCE', 'PROB CACHE'))

ax.legend((rects1[0], rects2[0], rects3[0], rects4[0], rects5[0], rects6[0], rects7[0], rects8[0], rects9[0]),
          ('requests_served_shortest_path_GEANT', 'requests_served_proposed_algorithm_GEANT','request_serving_potential_GEANT',
           'requests_served_shortest_path_GARR', 'requests_served_proposed_algorithm_GARR','request_serving_potential_GARR',
           'requests_served_shortest_path_WIDE', 'requests_served_proposed_algorithm_WIDE','request_serving_potential_WIDE'))

def autolabel(rects):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        """ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')"""

autolabel(rects1)
autolabel(rects2)
autolabel(rects3)
autolabel(rects4)
autolabel(rects5)
autolabel(rects6)
autolabel(rects7)
autolabel(rects8)
autolabel(rects9)

plt.show()
