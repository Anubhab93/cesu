import numpy as np
import matplotlib.pyplot as plt

N=4

pot_gt=(22.1, 21.8, 13.6, 23.5)
potStd_gt=(1.5, 1.4, 1.5, 1.2)
served_gt=(15.1, 13.1, 7.6, 16.1)
servedStd_gt=(0.9, 1.3, 0.9, 1.1)
myalgo_gt=(20.7, 20.6, 12.1, 21.6)
myalgoStd_gt=(0.6, 0.7, 0.6, 0.6)

pot_gr=(18.2, 16.9, 12.8, 19.1)
potStd_gr=(1.4, 1.5, 1.3, 1.5)
served_gr=(10.3, 10.5, 5.6, 10.3)
servedStd_gr=(0.9, 0.8, 0.9, 1.1)
myalgo_gr=(16.9, 15.9, 11.3, 17.6)
myalgoStd_gr=(0.5, 0.6, 0.4, 0.6)

pot_wd=(15.8, 15.2, 9.1, 12.3)
potStd_wd=(1.3, 1.4, 0.9, 0.8)
served_wd=(11.4, 10.9, 5.9, 9.1)
servedStd_wd=(0.8, 1.1, 0.7, 0.6)
myalgo_wd=(15, 13.9, 8.1, 11.5)
myalgoStd_wd=(0.5, 0.6, 0.4, 0.6)

ind = np.arange(N)
width = 0.07

fig, ax = plt.subplots()

rects1 = ax.bar(ind, served_gt, width, color='g', yerr=servedStd_gt, ecolor='k')
rects2 = ax.bar(ind+width, myalgo_gt, width, color='lawngreen', yerr=myalgoStd_gt, ecolor='k')
rects3 = ax.bar(ind+width+width, pot_gt, width, color='lightgreen', yerr=potStd_gt, ecolor='k')
rects4 = ax.bar(ind+width+width+width+width, served_gr, width, color='r', yerr=servedStd_gr, ecolor='k')
rects5 = ax.bar(ind+width+width+width+width+width, myalgo_gr, width, color='tomato', yerr=myalgoStd_gr, ecolor='k')
rects6 = ax.bar(ind+width+width+width+width+width+width, pot_gr, width, color='lightsalmon', yerr=potStd_gr, ecolor='k')
rects7 = ax.bar(ind+width+width+width+width+width+width+width+width, served_wd, width, color='darkcyan', yerr=servedStd_wd, ecolor='k')
rects8 = ax.bar(ind+width+width+width+width+width+width+width+width+width, myalgo_wd, width, color='navy', yerr=myalgoStd_wd, ecolor='k')
rects9 = ax.bar(ind+width+width+width+width+width+width+width+width+width+width, pot_wd, width, color='blue', yerr=potStd_wd, ecolor='k')

ax.set_ylim([0, 80])
ax.set_ylabel('Percentage of Served Requests')
#ax.set_xlabel('A')
ax.set_title('Content Universe=500K')
ax.set_xticks(ind+width+width+width+width+width+width)
ax.set_xticklabels(('CL4M', 'LCD', 'LCE', 'PROB CACHE'))

ax.legend((rects1[0], rects2[0], rects3[0], rects4[0], rects5[0], rects6[0], rects7[0], rects8[0], rects9[0]),
          ('requests_served_shortest_path_GEANT', 'requests_served_proposed_algorithm_GEANT','request_serving_potential_GEANT',
           'requests_served_shortest_path_GARR', 'requests_served_proposed_algorithm_GARR','request_serving_potential_GARR',
           'requests_served_shortest_path_WIDE', 'requests_served_proposed_algorithm_WIDE','request_serving_potential_WIDE'))

def autolabel(rects):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        """ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')"""

autolabel(rects1)
autolabel(rects2)
autolabel(rects3)
autolabel(rects4)
autolabel(rects5)
autolabel(rects6)
autolabel(rects7)
autolabel(rects8)
autolabel(rects9)

plt.show()
