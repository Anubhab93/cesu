import numpy as np
import matplotlib.pyplot as plt

N=4

pot_gt=(57.2, 58.8, 52.3, 61.4)
potStd_gt=(1.2, 1.1, 1.2, 1.1)
served_gt=(48.1, 18.8, 42.1, 53.5)
servedStd_gt=(0.9, 0.7, 0.6, 0.7)
myalgo_gt=(56.9, 58.1, 51.3, 60.1)
myalgoStd_gt=(0.5, 0.4, 0.5, 0.7)

pot_gr=(51.5, 53.5, 48.4, 54.4)
potStd_gr=(1.0, 1.2, 0.7, 1.1)
served_gr=(39.9, 42.1, 32.8, 44.4)
servedStd_gr=(0.5, 1.1, 0.4, 0.7)
myalgo_gr=(50.8, 52.6, 47.6, 53.7)
myalgoStd_gr=(0.5, 0.6, 0.4, 0.5)

pot_wd=(50.5, 42.1, 42.9, 49.0)
potStd_wd=(1.3, 1.1, 1.0, 1.0)
served_wd=(39.8, 34.2, 36.6, 44.5)
servedStd_wd=(0.9, 0.8, 0.8, 0.7)
myalgo_wd=(49.8, 41.6, 42.0, 48.2)
myalgoStd_wd=(0.4, 0.3, 0.4, 0.5)

ind = np.arange(N)
width = 0.07

fig, ax = plt.subplots()

rects1 = ax.bar(ind, served_gt, width, color='g', yerr=servedStd_gt, ecolor='k')
rects2 = ax.bar(ind+width, myalgo_gt, width, color='lawngreen', yerr=myalgoStd_gt, ecolor='k')
rects3 = ax.bar(ind+width+width, pot_gt, width, color='lightgreen', yerr=potStd_gt, ecolor='k')
rects4 = ax.bar(ind+width+width+width+width, served_gr, width, color='r', yerr=servedStd_gr, ecolor='k')
rects5 = ax.bar(ind+width+width+width+width+width, myalgo_gr, width, color='tomato', yerr=myalgoStd_gr, ecolor='k')
rects6 = ax.bar(ind+width+width+width+width+width+width, pot_gr, width, color='lightsalmon', yerr=potStd_gr, ecolor='k')
rects7 = ax.bar(ind+width+width+width+width+width+width+width+width, served_wd, width, color='darkcyan', yerr=servedStd_wd, ecolor='k')
rects8 = ax.bar(ind+width+width+width+width+width+width+width+width+width, myalgo_wd, width, color='navy', yerr=myalgoStd_wd, ecolor='k')
rects9 = ax.bar(ind+width+width+width+width+width+width+width+width+width+width, pot_wd, width, color='blue', yerr=potStd_wd, ecolor='k')

ax.set_ylim([0, 120])
ax.set_ylabel('Percentage of Served Requests')
#ax.set_xlabel('A')
ax.set_title('Alpha=1.0')
ax.set_xticks(ind+width+width+width+width+width+width)
ax.set_xticklabels(('CL4M', 'LCD', 'LCE', 'PROB CACHE'))

ax.legend((rects1[0], rects2[0], rects3[0], rects4[0], rects5[0], rects6[0], rects7[0], rects8[0], rects9[0]),
          ('requests_served_shortest_path_GEANT', 'requests_served_proposed_algorithm_GEANT','request_serving_potential_GEANT',
           'requests_served_shortest_path_GARR', 'requests_served_proposed_algorithm_GARR','request_serving_potential_GARR',
           'requests_served_shortest_path_WIDE', 'requests_served_proposed_algorithm_WIDE','request_serving_potential_WIDE'))

def autolabel(rects):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        """ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')"""

autolabel(rects1)
autolabel(rects2)
autolabel(rects3)
autolabel(rects4)
autolabel(rects5)
autolabel(rects6)
autolabel(rects7)
autolabel(rects8)
autolabel(rects9)

plt.show()
