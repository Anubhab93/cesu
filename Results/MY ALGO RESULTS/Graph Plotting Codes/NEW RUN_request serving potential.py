import numpy as np
import matplotlib.pyplot as plt
import math

al1=[48.45, 54.85]
al2=[67.25, 69.95]
al3=[85.8, 82.5]
al4=[87.7, 84.1]

b1=[0.20, 0.98]
b2=[1.43, 1.48]
b3=[0.3, 1.6]
b4=[0.71, 0.35]


ind = np.arange(2)
width = 0.10

fig, ax = plt.subplots()

rects1 = plt.bar(ind, al1, width,
                 color='g',yerr=b1, ecolor='k',
                 label='alpha=0.4')

rects2 = plt.bar(ind+width, al2, width,
                 color='y',yerr=b2, ecolor='k',
                 label='alpha=0.7')

rects3 = plt.bar(ind+width+width, al3, width,
                 color='r',yerr=b3, ecolor='k',
                 label='alpha=1.0')

rects4 = plt.bar(ind+width+width+width, al4, width,
                 color='b',yerr=b4, ecolor='k',
                 label='alpha=1.1')

"""rects5 = plt.bar(ind+width+width+width+width+width+width+width, al5, width,yerr=b5, ecolor='k',
                 color='g')

rects6 = plt.bar(ind+width+width+width+width+width+width+width+width, al6, width,
                 yerr=b6, ecolor='k',color='y')

rects7 = plt.bar(ind+width+width+width+width+width+width+width+width+width, al7, width,
                 yerr=b7, ecolor='k', color='r')

rects8 = plt.bar(ind+width+width+width+width+width+width+width+width+width+width, al8, width,
                 yerr=b8, ecolor='k', color='b')"""

"""plt.xlabel('Number of Requests Evaluated (in 10^5)')"""
plt.ylabel('Percentage')
plt.title('Number of Requests served as a percentage of Request Serving Potential (PROB CACHE)')
x1,x2,y1,y2 = plt.axis()
plt.axis((x1,x2,0,100))
plt.xticks(ind+width+width+width, ('Before Steady State Attained', 'After Steady State Attained'))
plt.legend()

def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')

autolabel(rects1)
autolabel(rects2)
autolabel(rects3)
autolabel(rects4)

plt.tight_layout()
plt.show()
