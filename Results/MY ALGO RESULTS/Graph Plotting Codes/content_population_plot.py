import numpy as np
import matplotlib.pyplot as plt
from pylab import *
import math

def write_into_file(list_tot, list_name, fo):
    #fo = open('E:\content_population.txt','w')
    fo.write("variable: %s\n"%str(list_name))
    for item in list_tot:
        fo.write("%s\n"%str(item))
    fo.write("\n\n\n")
    #fo.close()

locationStr='E:\FFOutput\icarus-0.5.0-anubhab\Results\MY ALGO RESULTS\NEW RUN (alpha=0.7 nc=0.5 cust=1 GEANT)\Content Population (Custodian Down)'

contentStr1='_content_population_1.txt'
contentStr2='_content_population_2.txt'
contentStr3='_content_population_3.txt'
contentStr4='_content_population_4.txt'

fname11=locationStr+ '\\cl4m'+ contentStr1
fname12=locationStr+ '\\cl4m'+ contentStr2
fname13=locationStr+ '\\cl4m'+ contentStr3
fname14=locationStr+ '\\cl4m'+ contentStr4

fname21=locationStr+ '\\lcd'+ contentStr1
fname22=locationStr+ '\\lcd'+ contentStr2
fname23=locationStr+ '\\lcd'+ contentStr3
fname24=locationStr+ '\\lcd'+ contentStr4

fname31=locationStr+ '\\lce'+ contentStr1
fname32=locationStr+ '\\lce'+ contentStr2
fname33=locationStr+ '\\lce'+ contentStr3
fname34=locationStr+ '\\lce'+ contentStr4

fname41=locationStr+ '\\prob_cache'+ contentStr1
fname42=locationStr+ '\\prob_cache'+ contentStr2
fname43=locationStr+ '\\prob_cache'+ contentStr3
fname44=locationStr+ '\\prob_cache'+ contentStr4

with open (fname11) as fo11:
    content11=fo11.readlines()
with open (fname12) as fo12:
    content12=fo12.readlines()
with open (fname13) as fo13:
    content13=fo13.readlines()
with open (fname14) as fo14:
    content14=fo14.readlines()

with open (fname21) as fo21:
    content21=fo21.readlines()
with open (fname22) as fo22:
    content22=fo22.readlines()
with open (fname23) as fo23:
    content23=fo23.readlines()
with open (fname24) as fo24:
    content24=fo24.readlines()

with open (fname31) as fo31:
    content31=fo31.readlines()
with open (fname32) as fo32:
    content32=fo32.readlines()
with open (fname33) as fo33:
    content33=fo33.readlines()
with open (fname34) as fo34:
    content34=fo34.readlines()

with open (fname41) as fo41:
    content41=fo41.readlines()
with open (fname42) as fo42:
    content42=fo42.readlines()
with open (fname43) as fo43:
    content43=fo43.readlines()
with open (fname44) as fo44:
    content44=fo44.readlines()

x=[]
x_tck=[]

cl4m=[]
lcd=[]
lce=[]
prob_cache=[]

se1=[]
se2=[]
se3=[]
se4=[]

for i in range(0,61):
    x.append(i)
    a=[]
    a.append(float(content11[i]))
    a.append(float(content12[i]))
    a.append(float(content13[i]))
    a.append(float(content14[i]))
    cl4m.append((np.mean(a))/10)
    se1.append((np.std(a))/10)
    b=[]
    b.append(float(content21[i]))
    b.append(float(content22[i]))
    b.append(float(content23[i]))
    b.append(float(content24[i]))
    lcd.append((np.mean(b))/10)
    se2.append((np.std(b))/10)
    c=[]
    c.append(float(content31[i]))
    c.append(float(content32[i]))
    c.append(float(content33[i]))
    c.append(float(content34[i]))
    lce.append((np.mean(c))/10)
    se3.append((np.std(c))/10)
    d=[]
    d.append(float(content41[i]))
    d.append(float(content42[i]))
    d.append(float(content43[i]))
    d.append(float(content44[i]))
    prob_cache.append((np.mean(d))/10)
    se4.append((np.std(d))/10)
    if i==0 or i%4==0:
        x_tck.append(i)

fo = open('E:\content_population.txt','a')
write_into_file(cl4m,"cl4m",fo)
fo.close()
fo = open('E:\content_population.txt','a')
write_into_file(se1,"se1",fo)
fo.close()
fo = open('E:\content_population.txt','a')
write_into_file(lcd,"lcd",fo)
fo.close()
fo = open('E:\content_population.txt','a')
write_into_file(se2,"se2",fo)
fo.close()
fo = open('E:\content_population.txt','a')
write_into_file(lce,"lce",fo)
fo.close()
fo = open('E:\content_population.txt','a')
write_into_file(se3,"se3",fo)
fo.close()
fo = open('E:\content_population.txt','a')
write_into_file(prob_cache,"prob_cache",fo)
fo.close()
fo = open('E:\content_population.txt','a')
write_into_file(se4,"se4",fo)
fo.close()



"""for cnt in content1:
    r=float(cnt)
    r=r/10
    cl4m.append(r)

for cnt in content2:
    r=float(cnt)
    r=r/10
    lcd.append(r)

for cnt in content3:
    r=float(cnt)
    r=r/10
    lce.append(r)

for cnt in content4:
    r=float(cnt)
    r=r/10
    prob_cache.append(r)"""


"""plt.plot(x,lcd,marker='*',color='g',label='LCD')
plt.plot(x,lce,marker='*',color='r',label='LCE')
plt.plot(x,cl4m,marker='*',color='b',label='CL4M')
plt.plot(x,prob_cache,marker='*',color='m',label='PROB_CACHE')
plt.axis([0, 6, 0, 60])
plt.xlabel('No. of requests evaluated (in 10^5)')
plt.ylabel('Percentage of Contents present in caches')
plt.title('Content Population_alpha_1.0_Network Cache_0.5')
ax.set_xticks(x_tck)
plt.legend()
plt.show()"""

"""print "cl4m: ", cl4m
print "lcd: ", lcd
print "lce: ",lce
print "prob_cache: ", prob_cache
print "se1: ", se1
print "se 2: ", se2
print "se3: ", se3
print "se4: ", se4"""

"""fig = plt.figure()
ax = fig.add_subplot(111)
ax.set_xlabel('No. of requests evaluated (in 2*10^3)', fontsize=18)
ax.set_ylabel('Percentage of Contents present in caches', fontsize=18)
#ax.set_title('Variation of Content Population when Server is under DoS attack')
ax.set_xticks(x_tck)

ax.axis([0, 60, 0, 30])
ax.plot(x, cl4m, color='b',label='CL4M')
ax.errorbar(x, cl4m, yerr = se1, ecolor = 'b')
ax.plot(x, lcd, color='g',label='LCD')
ax.errorbar(x, lcd, yerr = se2, ecolor = 'g')
ax.plot(x, lce, color='r',label='LCE')
ax.errorbar(x, lce, yerr = se3, ecolor = 'r')
ax.plot(x, prob_cache, color='m',label='PROB_CACHE')
ax.errorbar(x, prob_cache, yerr = se4, ecolor = 'm')
plt.legend()
plt.show()"""
