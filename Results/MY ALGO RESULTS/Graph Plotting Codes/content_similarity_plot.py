import numpy as np
import matplotlib.pyplot as plt
from pylab import *
import math

N=4

menMeans = []      #store 1st value in every mean file
menStd = []
womenMeans = []   #store 2nd value in every mean file
womenStd = []

cl4m_mean_values_server_available=[3.98, 3.54, 3.12, 4.45, 4.07]
cl4m_std_values1=[4.11, 4.02, 3.98, 4.31, 4.98]
cl4m_mean_values_server_unavailable=[7.83, 8.12, 8.67, 7.31, 7.94]
cl4m_std_values2=[8.53, 8.92, 7.67, 8.11, 8.54]

lcd_mean_values_server_available=[4.48, 4.67, 4.32, 4.18, 4.32]
lcd_std_values1=[5.19, 5.29, 4.76, 4.86, 4.94]
lcd_mean_values_server_unavailable=[10.73, 9.70, 10.50, 9.98, 10.60]
lcd_std_values2=[8.96, 8.35, 8.73, 8.65, 8.87]

lce_mean_values_server_available=[3.14, 2.74, 2.87, 3.33, 2.88]
lce_std_values1=[5.47, 5.46, 5.54, 5.79, 5.73]
lce_mean_values_server_unavailable=[11.03, 11.76, 11.50, 11.79, 12.36]
lce_std_values2=[9.03, 9.63, 9.37, 9.63, 9.99]

prob_cache_mean_values_server_available=[1.22, 1.22, 1.43, 1.56, 1.53]
prob_cache_std_values1=[1.50, 1.37, 1.64, 1.84, 1.70]
prob_cache_mean_values_server_unavailable=[4.13, 4.45, 4.32, 4.39, 4.35]
prob_cache_std_values2=[4.97, 5.15, 5.23, 5.23, 5.16]

menMeans.append(np.mean(cl4m_mean_values_server_available))
menMeans.append(np.mean(lcd_mean_values_server_available))
menMeans.append(np.mean(lce_mean_values_server_available))
menMeans.append(np.mean(prob_cache_mean_values_server_available))

womenMeans.append(np.mean(cl4m_mean_values_server_unavailable))
womenMeans.append(np.mean(lcd_mean_values_server_unavailable))
womenMeans.append(np.mean(lce_mean_values_server_unavailable))
womenMeans.append(np.mean(prob_cache_mean_values_server_unavailable))

menStd.append(np.mean(cl4m_std_values1))
menStd.append(np.mean(lcd_std_values1))
menStd.append(np.mean(lce_std_values1))
menStd.append(np.mean(prob_cache_std_values1))

womenStd.append(np.mean(cl4m_std_values2))
womenStd.append(np.mean(lcd_std_values2))
womenStd.append(np.mean(lce_std_values2))
womenStd.append(np.mean(prob_cache_std_values2))

ind = np.arange(N)  # the x locations for the groups
width = 0.20       # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(ind, menMeans, width, color='b', yerr=menStd, ecolor='r')
rects2 = ax.bar(ind + width, womenMeans, width, color='g', yerr=womenStd, ecolor='r')

# add some text for labels, title and axes ticks
ax.set_ylabel('Percentage of Content Similarity')
ax.set_title('Content Similarity among Caches')
ax.set_xticks(ind + width)
ax.set_xticklabels(('CL4M', 'LCD', 'LCE', 'PROB_CACHE'))

ax.legend((rects1[0], rects2[0]), ('Server is available', 'Server is unavailable'))


def autolabel(rects):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        """ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')"""

autolabel(rects1)
autolabel(rects2)

plt.show()


"""plt.plot(x,lcd,marker='*',color='g',label='LCD')
plt.plot(x,lce,marker='*',color='r',label='LCE')
plt.plot(x,cl4m,marker='*',color='b',label='CL4M')
plt.plot(x,prob_cache,marker='*',color='m',label='PROB_CACHE')
plt.axis([0, 6, 0, 60])
plt.xlabel('No. of requests evaluated (in 10^5)')
plt.ylabel('Percentage of Contents present in caches')
plt.title('Content Population_alpha_1.0_Network Cache_0.5')
ax.set_xticks(x_tck)
plt.legend()
plt.show()"""
