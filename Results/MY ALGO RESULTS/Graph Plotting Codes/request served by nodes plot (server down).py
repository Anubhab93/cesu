import numpy as np
import matplotlib.pyplot as plt
import math

locationStr='E:\FFOutput\icarus-0.5.0-anubhab\Results\MY ALGO RESULTS\Requests served by Caches (Server Down)'
contentStr='_served_by_nodes_alpha_06_cache_5000.txt'
fname1=locationStr+ '\\cl4m'+ contentStr
fname2=locationStr+ '\\my_algo_cl4m'+ contentStr

with open (fname1) as fo1:
    content1=fo1.readlines()
with open (fname2) as fo2:
    content2=fo2.readlines()

cl4m=[]
my_algo_cl4m=[]

c=0
d=0

for cnt in content1:
    r=float(cnt)
    r=r/1000
    s=r-c
    cl4m.append(s)
    c=r

for cnt in content2:
    r=float(cnt)
    r=r/1000
    s=r-d
    my_algo_cl4m.append(s)
    d=r


ind = np.arange(5)
#ind1= np.arange(0,1,0.2)
width = 0.10

fig, ax = plt.subplots()
"""
rects1 = ax.bar(ind, cl4m, width, color='g')
rects2 = ax.bar(ind + width, lcd, width, color='y')
rects3 = ax.bar(ind + width+width, lce, width, color='b')
rects4 = ax.bar(ind + width+width+width, prob_cache, width, color='m')
rects5 = ax.bar(ind + width+width+width+width, proposed_algo, width, color='r')

ax.set_ylabel('Number of Served Requests (in %age)')
ax.set_xlabel('Number of Requests (in 10^5)')
ax.set_title('ALPHA_0.6_CACHE_0.5')
ax.set_xticks(ind+width+width+0.20)
ax.set_xticklabels(('1', '2', '3', '4', '5'))
x1,x2,y1,y2 = plt.axis()
plt.axis((x1,x2,0,100))
ax.legend((rects1[0], rects2[0], rects3[0], rects4[0], rects5[0]), ('CL4M', 'LCD', 'LCE', 'PROB_CACHE', 'PROPOSED_ALGO'))

def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')

autolabel(rects1)
autolabel(rects2)
autolabel(rects3)
autolabel(rects4)
autolabel(rects5)
plt.show()
"""

rects1 = plt.bar(ind, cl4m, width,
                 color='g',
                 label='EXISTING STRATEGY')

rects2 = plt.bar(ind+width, my_algo_cl4m, width,
                 color='y',
                 label='PROPOSED ALGORITHM')


plt.xlabel('Number of Requests Evaluated (in 10^5)')
plt.ylabel('Number of Served Requests (in %age)')
plt.title('Requests Served by Network Caches for CL4M alpha=0.6 network cache=0.5')
x1,x2,y1,y2 = plt.axis()
plt.axis((x1,x2,0,100))
plt.xticks(ind+width+width+width, ('1', '2', '3', '4', '5'))
plt.legend()

def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')

autolabel(rects1)
autolabel(rects2)

plt.tight_layout()
plt.show()
