import numpy as np
import matplotlib.pyplot as plt
import math

"""fname1='E:\FFOutput\icarus-0.5.0-anubhab\Results\MY ALGO RESULTS\Requests served by Caches (Server Down)\prob_cache_served_by_nodes_alpha_06_cache_5000.txt'
fname2='E:\FFOutput\icarus-0.5.0-anubhab\Results\MY ALGO RESULTS\Requests served by Caches (Server Down)\my_algo_prob_cache_served_by_nodes_alpha_06_cache_5000.txt'

with open (fname1) as fo1:
    content1=fo1.readlines()
with open (fname2) as fo2:
    content2=fo2.readlines()

c=0
d=0

for cnt in content1:
    r=float(cnt)
    r=r/1000
    s=r-c
    cl4m.append(s)
    c=r

for cnt in content2:
    r=float(cnt)
    r=r/1000
    s=r-d
    my_algo_cl4m.append(s)
    d=r
"""

ind = np.arange(4)
#ind1= np.arange(0,1,0.2)
width = 0.10

fig, ax = plt.subplots()

existing=[28, 32, 24, 31]
potntl=[41, 38, 40, 52]
b1=[2.5, 2.5, 4, 1.5]
b2=[2.5, 2.5, 4, 1.5]

rects1 = plt.bar(ind, existing, width,
                 color='g',
                 label='REQUESTS SERVED',
                 yerr=b1, ecolor='k')

rects2 = plt.bar(ind+width, potntl, width,
                 color='b',
                 label='REQUEST SERVING POTENTIAL',
                 yerr=b2, ecolor='k')


"""plt.xlabel('Number of Requests Evaluated (in 10^5)')"""
plt.ylabel('Percentage of Served Requests', fontsize=20)
#plt.title('After Steady State is attained')
x1,x2,y1,y2 = plt.axis()
plt.axis((x1,x2,0,100))
plt.xticks(ind+width, ('CL4M', 'LCD', 'LCE', 'PROB_CACHE'), fontsize=20)
plt.legend(prop={'size':25})

def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
"""        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')"""

autolabel(rects1)
autolabel(rects2)

plt.tight_layout()
plt.show()
