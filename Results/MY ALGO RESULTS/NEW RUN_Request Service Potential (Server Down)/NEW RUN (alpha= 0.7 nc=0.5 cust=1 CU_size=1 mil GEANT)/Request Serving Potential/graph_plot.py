import numpy as np
import matplotlib.pyplot as plt

N = 4
vals = (28.2, 32.7, 24.2, 31.3)
stds = (2.3, 4.7, 4, 3.7)

ind = np.arange(N)  # the x locations for the groups
width = 0.25       # the width of the bars

fig, ax = plt.subplots()
rects1 = ax.bar(ind, vals, width, color='g', yerr=stds, ecolor='k')

vals2 = (41.3, 38.4, 40.5, 52.2)
stds2 = (2.7, 2.7, 7.1, 1.8)
rects2 = ax.bar(ind + width, vals2, width, color='b', yerr=stds2, ecolor='k')

# add some text for labels, title and axes ticks
ax.set_ylabel('Percentage of Requests')
ax.set_title('Request Serving Potential Network Cache size 0.005')
ax.set_xticks(ind + width)
ax.set_xticklabels(('CL4M', 'LCD', 'LCE', 'PROB_CACHE'))

ax.legend((rects1[0], rects2[0]), ('Requests Served', 'Request Serving Potential'))


def autolabel(rects):
    # attach some text labels
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')

autolabel(rects1)
autolabel(rects2)

plt.show()
