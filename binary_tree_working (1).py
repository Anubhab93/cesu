import networkx as nx
import numpy as np
import fnss

nodes=15
G=nx.DiGraph()
G.add_nodes_from(range(0,nodes-1))
G.add_edge(0,1)
G.add_edge(0,2)
G.add_edge(1,3)
G.add_edge(1,4)
G.add_edge(2,5)
G.add_edge(2,6)
G.add_edge(3,7)
G.add_edge(3,8)
G.add_edge(4,9)
G.add_edge(4,10)
G.add_edge(5,11)
G.add_edge(5,12)
G.add_edge(6,13)
G.add_edge(6,14)

nx.write_graphml(G,"binary_tree.graphml")
