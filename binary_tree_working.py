import networkx as nx
import numpy as np
import fnss

nodes=10
G=nx.DiGraph()
G.add_nodes_from(range(0,nodes-1))
G.add_edge(0,1)
G.add_edge(0,2)
G.add_edge(1,3)
G.add_edge(1,4)
G.add_edge(2,5)
G.add_edge(2,6)
G.add_edge(3,7)
G.add_edge(4,8)
G.add_edge(0,9)

nx.write_graphml(G,"binary_tree.graphml")

a = nx.topological_sort(G,nbunch=None,reverse=False)

receivers = []
sources = []
routers = []
for i in range(0,nodes-1):
    if G.degree(i) == 3:
        sources.append(i)
    if G.degree(i) == 1:
        receivers.append(i)
    else:
        routers.append(i)


G.graph['icr_candidates'] = set(routers)
    for v in sources:
        fnss.add_stack(G, v, 'source')
    for v in receivers:
        fnss.add_stack(G, v, 'receiver')
    for v in routers:
        fnss.add_stack(G, v, 'router')
    # set weights and delays on all links
    fnss.set_weights_constant(G, 1.0)
    fnss.set_delays_constant(G, delay, 'ms')
    # label links as internal
    for u, v in G.edges_iter():
        G.edge[u][v]['type'] = 'internal'
    return IcnTopology(G)
