class DepthTree:
    def __init__(self,item,left=None,right=None):
        self.item=item
        self.left=left
        self.right=right

    def depth(self):
        left_depth = self.left.depth() if self.left else 0
        right_depth = self.right.depth() if self.right else 0
        return max(left_depth, right_depth)+1




