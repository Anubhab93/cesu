import numpy as np
import matplotlib.pyplot as plt

N = 6
menMeans = (47.251, 52.892, 47.119, 48.521, 49.222, 50.641)

ind = np.arange(N)
width = 0.15       

fig, ax = plt.subplots()
rects1 = ax.bar(ind, menMeans, width, color='r')
#rects2 = ax.bar(ind + width, womenMeans, width, color='y')

ax.set_ylabel('network cache request serving potential (Percentage)')
ax.set_title('Network Cache Request Serving Potential (alpha=1.0 network cache=0.05)')
ax.set_xticks(ind)
ax.set_xticklabels(('CL4M', 'LCD', 'LCE', 'PROB_CACHE', 'RAND_BERNOULLI', 'RAND_CHOICE'))
x1,x2,y1,y2 = plt.axis()
plt.axis((x1,x2,0,100))
#ax.legend((rects1[0]), ('Men'))

def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
#        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
#                '%d' % int(height),
#                ha='center', va='bottom')

autolabel(rects1)
#autolabel(rects2)
plt.show()
