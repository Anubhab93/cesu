import numpy as np
import matplotlib.pyplot as plt

N = 10
menMeans = (2509, 2364, 2411, 2392, 2260, 2289, 2299, 2278, 2251, 2263)
womenMeans = (4756, 4726, 4761, 4624, 4793, 4742, 4743, 4729, 4736, 4739)
#childMeans = (29, 30, 35, 23, 21)

ind = np.arange(N)
width = 0.30       

fig, ax = plt.subplots()
rects1 = ax.bar(ind, menMeans, width, color='g')
rects2 = ax.bar(ind + width, womenMeans, width, color='r')
#rects3 = ax.bar(ind + width+width, childMeans, width, color='g')

ax.set_ylabel('Number of Served Requests')
ax.set_xlabel('Number of Requests (in 10^5)')
ax.set_title('LCD_ALPHA_0.2_CACHE_0.2')
ax.set_xticks(ind+width+width+0.10)
ax.set_xticklabels(('1', '2', '3', '4', '5','6','7','8','9','10'))
x1,x2,y1,y2 = plt.axis()
plt.axis((x1,x2,0,5000))
ax.legend((rects1[0], rects2[0]), ('Requests Served', 'Requests which could have been served'))

def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')

autolabel(rects1)
autolabel(rects2)
#autolabel(rects3)
plt.show()
