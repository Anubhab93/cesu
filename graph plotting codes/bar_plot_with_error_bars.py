import numpy as np
import matplotlib.pyplot as plt

N = 5
menMeans = (20, 35, 30, 35, 27)
menStd = (2, 3, 4, 1, 2)
#womenMeans = (25, 32, 34, 20, 25)

ind = np.arange(N)
width = 0.10       

fig, ax = plt.subplots()
rects1 = ax.bar(ind, menMeans, width, color='r', yerr=menStd)
#rects2 = ax.bar(ind + width, womenMeans, width, color='y')

ax.set_ylabel('Scores')
ax.set_title('Scores by group and gender')
ax.set_xticks(ind)
ax.set_xticklabels(('G1', 'G2', 'G3', 'G4', 'G5'))
x1,x2,y1,y2 = plt.axis()
plt.axis((x1,x2,0,100))
#ax.legend((rects1[0]), ('Men'))

def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
#        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
#                '%d' % int(height),
#                ha='center', va='bottom')

autolabel(rects1)
#autolabel(rects2)
plt.show()
