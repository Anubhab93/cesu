import numpy as np
import matplotlib.pyplot as plt

x=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
lcd=[14.0, 14.9, 14.4, 13.5, 14.5, 14.5, 14.4, 13.3, 14.1, 14.8, 13.5]
lce=[10.0, 9.8, 9.6, 9.1, 9.9, 9.5, 9.8, 8.9, 8.6, 10.3, 9.2]
cl4m=[11.5, 11.1, 11.1, 11.3, 11.7, 12.0, 11.5, 11.1, 10.8, 11.6, 11.6]

plt.plot(x,lcd,marker='*',color='g',label='LCD')
plt.plot(x,lce,marker='*',color='r',label='LCE')
plt.plot(x,cl4m,marker='*',color='b',label='CL4M')
plt.axis([0, 12, 0, 25])
plt.xlabel('No. of requests evaluated (in 10^5)')
plt.ylabel('Percentage of Contents present in caches')
plt.title('content_population_alpha_0.6_Cache_0.2')
plt.legend()
plt.show()
