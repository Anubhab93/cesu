import numpy as np
import matplotlib.pyplot as plt
import math

fname1='E:\FFOutput\icarus-0.5.0-anubhab\Results\Mean and Std values\lcd_mean_values_alpha_06_cache_8000.txt'
fname2='E:\FFOutput\icarus-0.5.0-anubhab\Results\Mean and Std values\lcd_std_values_alpha_06_cache_8000.txt'
#num_lines1 = sum(1 for line in open(fname1))
#num_lines2 = sum(1 for line in open(fname2))
#print "number of lines in file1: "
#print num_lines1
#print "number of lines in file2: "
#print num_lines2

with open (fname1) as fo1:
    content1=fo1.readlines()
with open (fname2) as fo2:
    content2=fo2.readlines()

x=[]
means=[]
stds=[]
means.append(0)
stds.append(0)
for cnt in content1:
    r=float(cnt)
    s=(r*100)/42
    #print s
    means.append(s)

for cnt in content2:
    r=float(cnt)
    stds.append(r)

print max(means)+max(stds)

len1=len(means)
len2=len(stds)
if len1==len2:
    for i in range(0,len1):
        x.append(i)
    ind = np.arange(len1)
    width = 0.10       

    fig, ax = plt.subplots()
    rects1 = ax.bar(ind, means, width, color='g', yerr=stds)
    #rects2 = ax.bar(ind + width, womenMeans, width, color='y')

    ax.set_ylabel('Percentage of Similar Contents')
    ax.set_xlabel('No. of Requests Executed (in 10^5)')
    ax.set_title('Content Similarity_LCD_alpha_0.6_network cache_0.8')
    ax.set_xticks(ind)
#    ax.set_xticklabels(('G1', 'G2', 'G3', 'G4', 'G5'))
#    x1,x2,y1,y2 = plt.axis()
    plt.axis((0,9,0,50))
    #ax.legend((rects1[0]), ('Men'))

    def autolabel(rects):
        for rect in rects:
            height = rect.get_height()
#        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
#                '%d' % int(height),
#                ha='center', va='bottom')

    autolabel(rects1)
    #autolabel(rects2)
    plt.show()
else:
    print "argument lengths not same..plot not possible"

