import numpy as np
import matplotlib.pyplot as plt

N = 3
menMeans = (21, 29, 21)
#womenMeans = (25, 32, 34, 20, 25)

ind = np.arange(N)
width = 0.30       

fig, ax = plt.subplots()
rects1 = ax.bar(ind, menMeans, width, color='r')
#rects2 = ax.bar(ind + width, womenMeans, width, color='y')

ax.set_ylabel('Content Availability (Percentage)')
ax.set_title('Content Availability in Network Caches')
ax.set_xticks(ind)
ax.set_xticklabels(('CL4M', 'LCD', 'LCE'))
x1,x2,y1,y2 = plt.axis()
plt.axis((x1,x2,0,100))
#ax.legend((rects1[0]), ('Men'))

def autolabel(rects):
    for rect in rects:
        height = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
                '%d' % int(height),
                ha='center', va='bottom')

autolabel(rects1)
#autolabel(rects2)
plt.show()
