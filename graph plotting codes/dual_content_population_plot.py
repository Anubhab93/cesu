import numpy as np
import matplotlib.pyplot as plt

x=[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
lcd_u=[41.4, 41.7, 42.6, 42.1, 41.2, 41.4, 42.1, 41.7, 42.0, 41.0, 41.0]
lce_u=[30.9, 31.6, 30.9, 32.2, 31.0, 32.2, 32.1, 32.1, 31.8, 31.7, 30.9]
cl4m_u=[32.7, 32.4, 31.9, 32.5, 31.8, 31.8, 32.6, 30.5, 32.2, 32.7, 32.0]
lce_d=[30.9, 16.5, 16.2, 16.2, 16.1, 16.1, 16.1, 16.1, 16.1, 16.1, 16.0]
lcd_d=[39.5, 17.0, 16.4, 16.1, 16.0, 15.9, 15.9, 15.8, 15.8, 15.8, 15.8]
cl4m_d=[32.5, 15.6, 15.4, 15.4, 15.4, 15.4, 15.3, 15.3, 15.3, 15.3, 15.3]

plt.plot(x,lcd_u,marker='*',color='g',label='LCD_UP')
plt.plot(x,lce_u,marker='*',color='r',label='LCE_UP')
plt.plot(x,cl4m_u,marker='*',color='b',label='CL4M_UP')
plt.plot(x,lcd_d,marker='*',color='g',linestyle='--',label='LCD_DOWN')
plt.plot(x,lce_d,marker='*',color='r',linestyle='--',label='LCE_DOWN')
plt.plot(x,cl4m_d,marker='*',color='b',linestyle='--',label='CL4M_DOWN')
plt.axis([0, 12, 0, 60])
plt.xlabel('No. of requests evaluated (in 10^5)')
plt.ylabel('Percentage of Contents present in caches')
plt.title('content_population_alpha_0.6_Cache_0.8')
plt.legend()
plt.show()
