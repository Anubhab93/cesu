import numpy as np
import matplotlib.pyplot as plt

N = 3
menMeans = (10, 14, 9)
#womenMeans = (25, 32, 34, 20, 25)

ind = np.arange(N)
width = 0.30
x=ind+width
plt.scatter(x,menMeans)
c = [1,3,2]
plt.errorbar(x,menMeans,yerr=c, linestyle="-")
plt.show()
