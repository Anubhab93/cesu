import numpy as np
import matplotlib.pyplot as plt
import math



fname1='E:\FFOutput\icarus-0.5.0-anubhab\Results\Potential Request Serving (Server Up)\cl4m_served_by_nodes_alpha_06_cache_8000.txt'
fname2='E:\FFOutput\icarus-0.5.0-anubhab\Results\Potential Request Serving (Server Up)\cl4m_content_presence_alpha_06_cache_8000.txt'
#means----no of requests actually served
#stds----no of requests that could have been served

with open (fname1) as fo1:
    content1=fo1.readlines()
with open (fname2) as fo2:
    content2=fo2.readlines()

means=[]
stds=[]
t=0
m=0
for cnt in content1:
    r=float(cnt)
    s=r/1000
    s=s-t
    t=r/1000
    means.append(s)

for cnt in content2:
    r=float(cnt)
    s=r/1000
    s=s-m
    m=r/1000
    stds.append(s)

#print means
#print stds
print max(means)
print max(stds)

len1=len(means)
len2=len(stds)

if len1==len2:
    ind = np.arange(len1)
    width = 0.30       

    fig, ax = plt.subplots()
    rects1 = ax.bar(ind, means, width, color='g')
    rects2 = ax.bar(ind + width, stds, width, color='b')
#rects3 = ax.bar(ind + width+width, childMeans, width, color='g')

    ax.set_ylabel('Number of Served Requests (in %age)')
    ax.set_xlabel('Number of Requests (in 10^5)')
    ax.set_title('CL4M_ALPHA_0.6_CACHE_0.8')
    ax.set_xticks(ind+width+width+0.20)
    ax.set_xticklabels(('1', '2', '3', '4', '5','6','7','8','9','10'))
    x1,x2,y1,y2 = plt.axis()
    plt.axis((x1,x2,0,100))
    ax.legend((rects1[0], rects2[0]), ('Requests Served', 'Requests which could have been served'))

    def autolabel(rects):
        for rect in rects:
            height = rect.get_height()
#           ax.text(rect.get_x() + rect.get_width()/2., 1.05*height,
#                '%d' % int(height),
#                ha='center', va='bottom')

    autolabel(rects1)
    autolabel(rects2)
#   autolabel(rects3)
    plt.show()
else:
    print "argument lengths not same..plot not possible"
