import numpy as np
import matplotlib.pyplot as plt
import math

fname1='E:\FFOutput\icarus-0.5.0-anubhab\Results\Before Steady State attained\lcd_server_up_steady_state_attained_alpha_02_cache_8000.txt'
fname2='E:\FFOutput\icarus-0.5.0-anubhab\Results\Before Steady State attained\lce_server_up_steady_state_attained_alpha_02_cache_8000.txt'


with open (fname1) as fo1:
    content1=fo1.readlines()
with open (fname2) as fo2:
    content2=fo2.readlines()


x=[]
lcd=[]
lce=[]

for cnt in content1:
    r=float(cnt)
    s=r/10
    lcd.append(s)

for cnt in content2:
    r=float(cnt)
    s=r/10
    lce.append(s)


print max(lcd)
print max(lce)

len1=len(lcd)
len2=len(lce)

print len1
print len2

if len1!=26:
    print "lcd values short"
elif len2!=26:
    print "lce values short"
else:
    for i in range(0,len1):
        x.append(i)
    plt.plot(x,lcd,marker='*',color='g',label='LCD')
    plt.plot(x,lce,marker='*',color='r',label='LCE')
    plt.axis([0, 25, 0, 60])
    plt.xticks(x)
    plt.xlabel('No. of requests evaluated (in multiple of 2*10^2)')
    plt.ylabel('Percentage of Contents present in caches')
    plt.title('steady state attainment_alpha_0.2_Cache_0.8')
    plt.legend()
    plt.show()
