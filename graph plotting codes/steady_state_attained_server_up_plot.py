import numpy as np
import matplotlib.pyplot as plt
import math

fname1='E:\FFOutput\icarus-0.5.0-anubhab\Results\How fast steady state attained\lcd_content_population_alpha_08_cache_2000.txt'
fname2='E:\FFOutput\icarus-0.5.0-anubhab\Results\How fast steady state attained\lce_how_fast_steady_state_attained_alpha_08_cache_2000.txt'
fname3='E:\FFOutput\icarus-0.5.0-anubhab\Results\How fast steady state attained\cl4m_content_population_alpha_08_cache_2000.txt'

with open (fname1) as fo1:
    content1=fo1.readlines()
with open (fname2) as fo2:
    content2=fo2.readlines()
with open (fname3) as fo3:
    content3=fo3.readlines()

x=[]
lcd=[]
lce=[]
cl4m=[]

for cnt in content1:
    r=float(cnt)
    s=r/10
    lcd.append(s)

for cnt in content2:
    r=float(cnt)
    s=r/10
    lce.append(s)

for cnt in content3:
    r=float(cnt)
    s=r/10
    cl4m.append(s)

lcd=lcd[0:21]
lce=lce[0:21]
cl4m=cl4m[0:21]
print max(lcd)
print max(lce)
print max(cl4m)

len1=len(lcd)
len2=len(lce)
len3=len(cl4m)

print len1
print len2
print len3

if len1!=21:
    print "lcd values short"
elif len2!=21:
    print "lce values short"
elif len3!=21:
    print "cl4m values short"
else:
    for i in range(0,len1):
        x.append(i)
    plt.plot(x,lcd,marker='*',color='g',label='LCD')
    plt.plot(x,lce,marker='*',color='r',label='LCE')
    plt.plot(x,cl4m,marker='*',color='b',label='CL4M')
    plt.axis([0, 20, 0, 30])
    plt.xticks(x)
    plt.xlabel('No. of requests evaluated (in multiple of 10^3)')
    plt.ylabel('Percentage of Contents present in caches')
    plt.title('steady state attainment_alpha_0.8_Cache_0.2')
    plt.legend()
    plt.show()
