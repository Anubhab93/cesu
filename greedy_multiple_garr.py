# THIS WORKS IF ICARUS USES SAME SHORTEST PATH
import networkx as nx
import numpy as np
import fnss
import random
import csv
from igraph import *
from networkx.algorithms import tree
from operator import add
from operator import mul
from collections import OrderedDict
############################################################
G1=nx.read_graphml('Garr201201.graphml')
print G1.number_of_edges()
nodes = len(G1.nodes())
print nodes
contents = 40000
cache = 200
alpha = 0.8
name = str(alpha)
name2 = name.replace(".","")
name3 = str(cache)
name4 = str.join('%s'%name2,'%s'%name3)
#print name4
###########################################################
#get the receivers
deg = nx.degree(G1)
receiver11 = ['1', '7', '8', '9', '11', '12', '19', '26', '28', '30', '32', '33', '41', '42', '43', '47', '48', '50', '53', '57', '60']
receiver1 = ['1', '7', '8', '9', '11', '12', '19', '26', '28', '30', '32', '33', '41', '42', '43', '47', '48', '50', '53', '57', '60']
receiver2 = [1, 7, 8, 9, 11, 12, 19, 26, 28, 30, 32, 33, 41, 42, 43, 47, 48, 50, 53, 57, 60]
#print receiver2
#source_attachments = [v for v in G1.nodes() if deg[v] == 2]
deg2 = OrderedDict(sorted(deg.items(), key=lambda t: t[1], reverse=True))
cust_no = 2
custodian1 = []
custodian2 = []
#receiver1 = []
G=nx.DiGraph()
G2 = nx.DiGraph()
Gnew=Graph(directed=True)
dummy_custodian = []
for i in range(cust_no):
    c1 = deg2.items()[i]
    dummy = str(int(c1[0])+100)
    dummy_custodian.append(int(c1[0]))
    custodian1.append(dummy)
    custodian2.append(int(dummy))
    G.add_node(int(dummy))
    G.add_node(int(c1[0]))
    G2.add_node(int(dummy))
    G2.add_node(int(c1[0]))
    G1.add_node(dummy)
    G1.add_edge(c1[0],dummy)
    G.add_edge(int(c1[0]),int(dummy))
    G2.add_edge(int(c1[0]),int(dummy))
    Gnew.add_vertices(int(c1[0]))
    Gnew.add_vertices(int(dummy))
    Gnew.add_edge(int(c1[0]),int(dummy))
content_matrix = [[] for y in range(cust_no)]
#print dummy_custodian
###########################################################
# Content distribution
seed = 25
random.seed(seed)
for i in range(1, contents+1):
    source = random.choice(custodian1)
    #print source
    ind = custodian1.index(source)
    content_matrix[ind].append(i)
#print content_matrix
##########################################################
# router selection, incoming traffic matrix, & link_content matrix
routers = []
traffic_set = [[] for y in range(cust_no)]
link_content = [[] for y in range(contents+1)]
#print link_content
with open("path.txt" , 'r') as f:
    lines = f.readlines()
for i in range(0,len(lines)):
    x2 = lines[i]
    x2 = x2.replace("[","")
    x2 = x2.replace("]:\n","")
    path = x2.split(",")
    #print path
    cust = path[len(path)-1]
    #print cust
    j = dummy_custodian.index(int(cust))
    content1 = content_matrix[j][:]
    #print content1
    for k in range(len(path)):
	u = int(path[k])
	if u not in traffic_set[j]:
	    traffic_set[j].append(u)
	if u not in G.nodes():
	    G.add_node(u)
	    G2.add_node(u)
	    Gnew.add_vertices(u)
    for l in range(len(path)-1):
	u = int(path[l])
	v = int(path[l+1])
	c = (u,v)
	if c not in Gnew.get_edgelist():
	    Gnew.add_edge(u,v)
	    G2.add_edge(u,v)
	    #print ind
	    link_content[0].append(c)
	ind = link_content[0].index(c)
	for y in range(1,contents+1):
	    len(link_content[y])
	    if len(link_content[y]) < len(link_content[0]):
		link_content[y].append(0)
	for y in range(1,contents+1):
	    if y in content1:
		link_content[y][ind] = 1
    for n in range(1,len(path)):
	u = int(path[n])
	if u not in routers:
	    routers.append(u)
#print routers
#print traffic_set
#print np.shape(link_content)
#nx.write_graphml(G2,"Geant_test2.graphml")
##########################################################
# DAG creation
ini_edges = Gnew.get_edgelist()
#print ini_edges
del_list = Gnew.feedback_arc_set(weights=None, method="eades")
#print del_list
for i in range(len(del_list)):
    c = ini_edges[del_list[i]]
    #print c
    Gnew.delete_edges([c])
    ind2 = link_content[0].index(c)
    for j in range(contents+1):
	del link_content[j][ind2]
#print link_content
final_edges = Gnew.get_edgelist()
#print final_edges
#print link_content[0]
#print final_edges
for j in range(len(final_edges)):
    e1 = final_edges[j]
    u = int(e1[0])
    v = int(e1[1])
    G.add_edge(u,v)
#print routers
##########################################################
# Topological sort
topo_sort = nx.topological_sort(G,nbunch=None,reverse=False)
#print a2
# remove custodian from topo_sort
for i in range(cust_no):
    topo_sort.remove(custodian2[i])
##########################################################
# create cache matrix and content pdf
cache_mat = [[0 for x in range(contents)] for y in range(nodes)]
pdf = np.arange(1.0, contents+1.0)**-alpha
pdf /= np.sum(pdf)
#print pdf
##########################################################
# create incoming link and outgoing link matrix
incoming_link = [[0 for x in range(nodes)] for y in range(nodes)]
outgoing_link = [[0 for x in range(nodes)] for y in range(nodes)]
for i in range(len(final_edges)):
    e2 = final_edges[i]
    u = int(e2[0])
    v = int(e2[1])
    if v not in custodian2:
        incoming_link[v][u] = 1
	outgoing_link[u][v] = 1
#print incoming_link
#print outgoing_link
###########################################################
# FINAL PHASE: create cache matrix
for i in range(len(topo_sort)):
    node = topo_sort[i]
    node_traffic = incoming_link[node][:]
    ind = [i for i, x in enumerate(node_traffic) if x == 1]
    node_data = [0 for x in range(contents)]
    for j in range(len(ind)):
	u = ind[j]
	link = (u,node)
	ind2 = link_content[0].index(link)
	#print ind2
	link_data = []
	for k in range(1,contents+1):
	    link_data.append(link_content[k][ind2])
	node_data = map(add,node_data,link_data)
    pdf_node_data = map(mul,pdf,node_data)
    #print node
    #print pdf_node_data
    indexes = sorted(range(len(pdf_node_data)), key=lambda x: pdf_node_data[x])[-cache:]
    temp1 = [0 for x in range(contents)]
    if node not in receiver2:
    	for k in range(len(indexes)):
	    ind2 = indexes[k]
	    temp1[ind2] = 1
    cache_mat[node] = temp1
    for j in range(contents):
	if cache_mat[node][j] == 1:
	    node_data[j] = 0
    #print node_data
    if node not in receiver2:
	node_out = outgoing_link[node][:]
	ind3 = [i for i, x in enumerate(node_out) if x == 1]
	for j in range(len(ind3)):
	    v = ind3[j]
	    link = (node,v)
	    ind4 = link_content[0].index(link)
	    for k in range(contents):
		link_content[k+1][ind4] = int(link_content[k+1][ind4])*int(node_data[k])
#print cache_mat
with open('%s.csv'%name4,"wb") as f:
    writer = csv.writer(f)
    writer.writerows(cache_mat)

#with open('outgoing.csv',"wb") as f:
#    writer = csv.writer(f)
#    writer.writerows(outgoing_link)

#with open('link.csv',"wb") as f:
#    writer = csv.writer(f)
#    writer.writerows(link_content)
