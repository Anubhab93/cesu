@register_strategy('LCD')
class LeaveCopyDown(Strategy):
    """Leave Copy Down (LCD) strategy.
    
    According to this strategy, one copy of a content is replicated only in
    the caching node you hop away from the serving node in the direction of
    the receiver. This strategy is described in [2]_.
    
    Rereferences
    ------------
    ..[2] N. Laoutaris, H. Che, i. Stavrakakis, The LCD interconnection of LRU
          caches and its analysis. 
          Available: http://cs-people.bu.edu/nlaout/analysis_PEVA.pdf 
    """

    @inheritdoc(Strategy)
    def __init__(self, view, controller, **kwargs):
        super(LeaveCopyDown, self).__init__(view, controller)
	self.s_by_cache=0
	self.s_by_potential=0
	self.s_by_breadcrumb=0
	self.s_by_random=0
	self.node_content_presence_table=[[[0 for x in range(1)] for y in range(100000)] for z in range(30)]
	#self.node_breadcrumb_table= np.zeros((40,1000,1))
	self.node_breadcrumb_table=[[[-1 for x in range(1)] for y in range(100000)] for z in range(30)]
	#self.node_connection_matrix= np.zeros((40,40))
	"""the next part is used to get the undirected graph"""
	topology_graph = fnss.parse_topology_zoo(path.join(TOPOLOGY_RESOURCES_DIR, 'Garr201201.graphml')).to_undirected()

    @inheritdoc(Strategy)
    def process_event(self, time, receiver, content, log):
        n=100000
        source = self.view.content_source(content)
        #print "source of the content is: ",source
        path = self.view.shortest_path(receiver, source)
        # Route requests to original source and queries caches on the path
        self.controller.start_session(time, receiver, content, log)
        if self.count<=2000000:
            for u, v in path_links(path):
                self.controller.forward_request_hop(u, v)
                if self.view.has_cache(v):
                    if self.controller.get_content(v):
                        serving_node = v
                        break
            else:
            # No cache hits, get content from source
                self.controller.get_content(v)
                serving_node = v
            # Return content
            path = list(reversed(self.view.shortest_path(receiver, serving_node)))
            # Leave a copy of the content only in the cache one level down the hit
            # caching node
            copied = False
            for u, v in path_links(path):
                self.controller.forward_content_hop(u, v)
                if not copied and v != receiver and self.view.has_cache(v):
                    self.controller.put_content(v)
                    copied = True
        else:
            #fo1.write('\ncount is: %d\n'%self.count)
            #fo1.write('source of content is: %d\n'%source)
            if self.count==2000001:
                #fo1.write('finalizing caches..')
                for content in range(1,n+1):
                    #fo1.write('content id is: %d\n'%content)
                    #print "content is: ",content
                    caches=[]
                    caches=self.view.content_locations(content)
                    #fo1.write('sources are: %s'%caches)
                    #print "caches for this content are: ",caches
                    for cache in caches:
                        if int(cache)<1000:
                            #print "cache is: ",cache
                            #print "content is: ",content
                            self.node_content_presence_table[cache][content-1][0]=1
                            #print "content added to this cache..\n\n"

            #fo1.write('\n\n\n\n')
            hopCount=0
            nextNode=receiver
            serving_node= source
            #sFlag= False
            #rFlag= False
            while hopCount<15:
                hopCount=hopCount+1
                if self.node_content_presence_table[nextNode][content-1][0]==1:
                    serving_node=nextNode
                    break
                elif self.node_breadcrumb_table[nextNode][content-1][0]>-1:
                    nNode = self.node_breadcrumb_table[nextNode][content-1][0]
                    nextNode = nNode
                else:
                    no_of_neighbours = len(topology.neighbours(nextNode))
                    #print "conSetLen is: ",conSetLen
                    if no_of_neighbours == 0:
                        #print "no element in conSet.."
                        nextNode=2000
                    elif no_of_neighbours == 1:
                        #print "only 1 element in conset. that is: ",conSet[0]
                        nextNode=topology.neighbours(nextNode)[0]
                    else:
                        #print "more than 1 element in conSet.."
                        r=random.randint(0,len(topology.neighbours(nextNode)))
                        nextNode=topology_graph.neighbours(nextNode)[r]
                    if nextNode==2000:
                        #fo1.write('next node is 2000. terminating\n')
                        break
            #print "hopcount section over.."
            #fo1.write('hop count section over...\n\n\n')
            # Return content
            path = list(reversed(self.view.shortest_path(receiver, serving_node)))
            #calculation of request serving potential
            if self.count>2000000:
                caches=[]
                caches=self.view.content_locations(content)
                for cache in caches:
                    if int(cache)<1000:
                        self.s_by_potential+=1
                        break
            if serving_node<1000:
                self.s_by_cache+=1
                pathLen= len(path)
                if pathLen>=2:
                    #write code to update breadcrumb table
                    i=0
                    j=1
                    while j<pathLen:
                        if path[i]<1000 and path[j]<1000:
                            self.node_breadcrumb_table[path[j]][content-1][0]=path[i]
                        i+=1
                        j+=1
        if self.count>2000000:
            if self.count%100000==0:
                fo= open('my_algo_lcd_served_by_nodes_alpha.txt','a')
                fo.write('served_my algo: %d'%self.s_by_cache)
                fo.write('    potential: %d\n'%self.s_by_potential)
                fo.close()
                self.s_by_cache=0
                self.s_by_potential=0
        self.count+=1
        #fo1.close()
        self.controller.end_session()
