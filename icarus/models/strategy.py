"""Implementations of all caching and routing strategies
"""
from __future__ import division
import random
import abc
import collections
import csv

import networkx as nx
import numpy as np

from icarus.registry import register_strategy
from icarus.util import inheritdoc, multicast_tree, path_links
from icarus.execution.network import NetworkView


__all__ = [
       'Strategy',
       'Hashrouting',
       'HashroutingSymmetric',
       'HashroutingAsymmetric',
       'HashroutingMulticast',
       'HashroutingHybridAM',
       'HashroutingHybridSM',
       'NoCache',
       'Edge',
       'LeaveCopyEverywhere',
       'LeaveCopyDown',
       'CacheLessForMore',
       'RandomBernoulli',
       'RandomChoice',
       'RandomSearchAlgorithm',
       'NearestReplicaRouting',
           ]

#TODO: Implement BaseOnPath to reduce redundant code
#TODO: In Hashrouting, implement request routing phase under in single function
#see LCD for the code

def file_name(nameStrategy,nameAlpha,nameCache):
    try:
        nameAlpha="alpha_"+nameAlpha
        nameCache="cache_"+nameCache
        fname=nameStrategy+"_"+nameAlpha+"_"+nameCache
        fname=fname.replace(".","",2)
        return fname
    except SystemError:
        print "error occurred while naming"

def chooseRandNode(conMat, node):
    try:
        conSet=[]
        matLen=len(conMat[0])
        for i in range(0,40):
            if i!=node:
                if conMat[node][i]==1:
                    conSet.append(i)
        conSetLen=len(conSet)
        if conSetLen==0:
            return 2000
        else:
            r=random.randint(1,conSetLen)
            return conSet[r]
    except SystemError:
        print "error occurred while choosing random node"

class Strategy(object):
    """Base strategy imported by all other strategy classes"""

    __metaclass__ = abc.ABCMeta

    def __init__(self, view, controller, **kwargs):
        """Constructor

        Parameters
        ----------
        view : NetworkView
            An instance of the network view
        controller : NetworkController
            An instance of the network controller
        kwargs : keyworded arguments, optional
            Additional strategy parameters
        """
        self.view = view
        self.controller = controller

    @abc.abstractmethod
    def process_event(self, time, receiver, content, log):
        """Process an event received from the simulation engine.

        This event is processed by executing relevant actions of the network
        controller, potentially based on the current status of the network
        retrieved from the network view.

        Parameters
        ----------
        time : int
            The timestamp of the event
        receiver : any hashable type
            The receiver node requesting a content
        content : any hashable type
            The content identifier requested by the receiver
        log : bool
            Indicates whether the event must be registered by the data
            collectors attached to the network.
        """
        raise NotImplementedError('The selected strategy must implement '
                                  'a process_event method')



class Hashrouting(Strategy):
    """Base class for all hash-routing implementations. Hash-routing
    implementations are described in [1]_.

    References
    ----------
    .. [1] L. Saino, I. Psaras and G. Pavlou, Hash-routing Schemes for
    Information-Centric Networking, in Proceedings of ACM SIGCOMM ICN'13
    workshop. Available:
    https://www.ee.ucl.ac.uk/~lsaino/publications/hashrouting-icn13.pdf

    """

    @inheritdoc(Strategy)
    def __init__(self, view, controller, **kwargs):
        super(Hashrouting, self).__init__(view, controller)
        self.cache_nodes = view.cache_nodes()
        # Allocate results of hash function to caching nodes
        self.cache_assignment = dict((i, self.cache_nodes[i])
                                      for i in range(len(self.cache_nodes)))

    def authoritative_cache(self, content):
        """Return the authoritative cache node for the given content

        Parameters
        ----------
        content : any hashable type
            The identifier of the content

        Returns
        -------
        authoritative_cache : any hashable type
            The node on which the authoritative cache is deployed
        """
        return self.cache_assignment[self.hash(content)]

    def hash(self, content):
        """Return a hash code of the content for hash-routing purposes


        Parameters
        ----------
        content : any hashable type
            The identifier of the content

        Returns
        -------
        hash : int
            The hash code of the content
        """
        #TODO: This hash function needs revision because it does not return
        # equally probably hash codes
        n = len(self.cache_nodes)
        h = content % n
        return h if (content/n) % 2 == 0 else (n - h - 1)


@register_strategy('HR_SYMM')
class HashroutingSymmetric(Hashrouting):
    """Hash-routing with symmetric routing (HR SYMM)
    """

    @inheritdoc(Strategy)
    def __init__(self, view, controller, **kwargs):
        super(HashroutingSymmetric, self).__init__(view, controller)

    @inheritdoc(Strategy)
    def process_event(self, time, receiver, content, log):
        # get all required data
        source = self.view.content_source(content)
        cache = self.authoritative_cache(content)
        # handle (and log if required) actual request
        self.controller.start_session(time, receiver, content, log)
        # Forward request to authoritative cache
        self.controller.forward_request_path(receiver, cache)
        if self.controller.get_content(cache):
            # We have a cache hit here
            self.controller.forward_content_path(cache, receiver)
        else:
            # Cache miss: go all the way to source
            self.controller.forward_request_path(cache, source)
            if not self.controller.get_content(source):
                raise RuntimeError('The content is not found the expected source')
            self.controller.forward_content_path(source, cache)
            # Insert in cache
            self.controller.put_content(cache)
            # Forward to receiver
            self.controller.forward_content_path(cache, receiver)
        self.controller.end_session()


@register_strategy('HR_ASYMM')
class HashroutingAsymmetric(Hashrouting):
    """Hash-routing with asymmetric routing (HR ASYMM)
    """

    @inheritdoc(Strategy)
    def __init__(self, view, controller, **kwargs):
        super(HashroutingAsymmetric, self).__init__(view, controller)

    @inheritdoc(Strategy)
    def process_event(self, time, receiver, content, log):
        # get all required data
        source = self.view.content_source(content)
        cache = self.authoritative_cache(content)
        # handle (and log if required) actual request
        self.controller.start_session(time, receiver, content, log)
        # Forward request to authoritative cache
        self.controller.forward_request_path(receiver, cache)
        if self.controller.get_content(cache):
            # We have a cache hit here
            self.controller.forward_content_path(cache, receiver)
        else:
            # Cache miss: go all the way to source
            self.controller.forward_request_path(cache, source)
            if not self.controller.get_content(source):
                raise RuntimeError('The content was not found at the expected source')
            if cache in self.view.shortest_path(source, receiver):
                # Forward to cache
                self.controller.forward_content_path(source, cache)
                # Insert in cache
                self.controller.put_content(cache)
                # Forward to receiver
                self.controller.forward_content_path(cache, receiver)
            else:
                # Forward to receiver straight away
                self.controller.forward_content_path(source, receiver)
        self.controller.end_session()


@register_strategy('HR_MULTICAST')
class HashroutingMulticast(Hashrouting):
    """
    Hash-routing implementation with multicast delivery of content packets.

    In this strategy, if there is a cache miss, when contant packets return in
    the domain, the packet is multicast, one copy being sent to the
    authoritative cache and the other to the receiver. If the cache is on the
    path from source to receiver, this strategy behaves as a normal symmetric
    hash-routing strategy.
    """

    @inheritdoc(Strategy)
    def __init__(self, view, controller, **kwargs):
        super(HashroutingMulticast, self).__init__(view, controller)
        # map id of content to node with cache responsibility

    @inheritdoc(Strategy)
    def process_event(self, time, receiver, content, log):
        # get all required data
        source = self.view.content_source(content)
        cache = self.authoritative_cache(content)
        # handle (and log if required) actual request
        self.controller.start_session(time, receiver, content, log)
        # Forward request to authoritative cache
        self.controller.forward_request_path(receiver, cache)
        if self.controller.get_content(cache):
            # We have a cache hit here
            self.controller.forward_content_path(cache, receiver)
        else:
            # Cache miss: go all the way to source
            self.controller.forward_request_path(cache, source)
            if not self.controller.get_content(source):
                raise RuntimeError('The content is not found the expected source')
            if cache in self.view.shortest_path(source, receiver):
                self.controller.forward_content_path(source, cache)
                # Insert in cache
                self.controller.put_content(cache)
                # Forward to receiver
                self.controller.forward_content_path(cache, receiver)
            else:
                # Multicast
                cache_path = self.view.shortest_path(source, cache)
                recv_path = self.view.shortest_path(source, receiver)

                # find what is the node that has to fork the content flow
                for i in range(1, min([len(cache_path), len(recv_path)])):
                    if cache_path[i] != recv_path[i]:
                        fork_node = cache_path[i-1]
                        break
                else: fork_node = cache
                self.controller.forward_content_path(source, fork_node, main_path=True)
                self.controller.forward_content_path(fork_node, receiver, main_path=True)
                self.controller.forward_content_path(fork_node, cache, main_path=False)
                self.controller.put_content(cache)
        self.controller.end_session()


@register_strategy('HR_HYBRID_AM')
class HashroutingHybridAM(Hashrouting):
    """
    Hash-routing implementation with hybrid asymmetric-multicast delivery of
    content packets.

    In this strategy, if there is a cache miss, when content packets return in
    the domain, the packet is delivered to the receiver following the shortest
    path. If the additional number of hops required to send a copy to the
    authoritative cache is below a specific fraction of the network diameter,
    then one copy is sent to the authoritative cache as well. If the cache is
    on the path from source to receiver, this strategy behaves as a normal
    symmetric hash-routing strategy.
    """

    @inheritdoc(Strategy)
    def __init__(self, view, controller, max_stretch=0.2):
        super(HashroutingHybridAM, self).__init__(view, controller)
        self.max_stretch = nx.diameter(view.topology()) * max_stretch

    @inheritdoc(Strategy)
    def process_event(self, time, receiver, content, log):
        # get all required data
        source = self.view.content_source(content)
        cache = self.authoritative_cache(content)
        # handle (and log if required) actual request
        self.controller.start_session(time, receiver, content, log)
        # Forward request to authoritative cache
        self.controller.forward_request_path(receiver, cache)
        if self.controller.get_content(cache):
            # We have a cache hit here
            self.controller.forward_content_path(cache, receiver)
        else:
            # Cache miss: go all the way to source
            self.controller.forward_request_path(cache, source)
            if not self.controller.get_content(source):
                raise RuntimeError('The content was not found at the expected source')

            if cache in self.view.shortest_path(source, receiver):
                # Forward to cache
                self.controller.forward_content_path(source, cache)
                # Insert in cache
                self.controller.put_content(cache)
                # Forward to receiver
                self.controller.forward_content_path(cache, receiver)
            else:
                # Multicast
                cache_path = self.view.shortest_path(source, cache)
                recv_path = self.view.shortest_path(source, receiver)

                # find what is the node that has to fork the content flow
                for i in range(1, min([len(cache_path), len(recv_path)])):
                    if cache_path[i] != recv_path[i]:
                        fork_node = cache_path[i-1]
                        break
                else: fork_node = cache
                self.controller.forward_content_path(source, receiver, main_path=True)
                # multicast to cache only if stretch is under threshold
                if len(self.view.shortest_path(fork_node, cache)) - 1 < self.max_stretch:
                    self.controller.forward_content_path(fork_node, cache, main_path=False)
                    self.controller.put_content(cache)
        self.controller.end_session()


@register_strategy('HR_HYBRID_SM')
class HashroutingHybridSM(Hashrouting):
    """
    Hash-routing implementation with hybrid symmetric-multicast delivery of
    content packets.

    In this implementation, the edge router receiving a content packet decides
    whether to deliver the packet using multicast or symmetric hash-routing
    based on the total cost for delivering the Data to both cache and receiver
    in terms of hops.
    """

    @inheritdoc(Strategy)
    def __init__(self, view, controller, **kwargs):
        super(HashroutingHybridSM, self).__init__(view, controller)

    @inheritdoc(Strategy)
    def process_event(self, time, receiver, content, log):
        # get all required data
        source = self.view.content_source(content)
        cache = self.authoritative_cache(content)
        # handle (and log if required) actual request
        self.controller.start_session(time, receiver, content, log)
        # Forward request to authoritative cache
        self.controller.forward_request_path(receiver, cache)
        if self.controller.get_content(cache):
            # We have a cache hit here
            self.controller.forward_content_path(cache, receiver)
        else:
            # Cache miss: go all the way to source
            self.controller.forward_request_path(cache, source)
            if not self.controller.get_content(source):
                raise RuntimeError('The content is not found the expected source')

            if cache in self.view.shortest_path(source, receiver):
                self.controller.forward_content_path(source, cache)
                # Insert in cache
                self.controller.put_content(cache)
                # Forward to receiver
                self.controller.forward_content_path(cache, receiver)
            else:
                # Multicast
                cache_path = self.view.shortest_path(source, cache)
                recv_path = self.view.shortest_path(source, receiver)

                # find what is the node that has to fork the content flow
                for i in range(1, min([len(cache_path), len(recv_path)])):
                    if cache_path[i] != recv_path[i]:
                        fork_node = cache_path[i-1]
                        break
                else: fork_node = cache

                symmetric_path_len = len(self.view.shortest_path(source, cache)) + \
                                     len(self.view.shortest_path(cache, receiver)) - 2
                multicast_path_len = len(self.view.shortest_path(source, fork_node)) + \
                                     len(self.view.shortest_path(fork_node, cache)) + \
                                     len(self.view.shortest_path(fork_node, receiver)) - 3

                self.controller.put_content(cache)
                # If symmetric and multicast have equal cost, choose symmetric
                # because of easier packet processing
                if symmetric_path_len <= multicast_path_len: # use symmetric delivery
                    # Symmetric delivery
                    self.controller.forward_content_path(source, cache, main_path=True)
                    self.controller.forward_content_path(cache, receiver, main_path=True)
                else:
                    # Multicast delivery
                    self.controller.forward_content_path(source, receiver, main_path=True)
                    self.controller.forward_content_path(fork_node, cache, main_path=False)
                self.controller.end_session()


@register_strategy('NO_CACHE')
class NoCache(Strategy):
    """Strategy without any caching

    This corresponds to the traffic in a normal TCP/IP network without any
    CDNs or overlay caching, where all content requests are served by the
    original source.
    """

    @inheritdoc(Strategy)
    def __init__(self, view, controller, **kwargs):
        super(NoCache, self).__init__(view, controller)

    @inheritdoc(Strategy)
    def process_event(self, time, receiver, content, log):
        # get all required data
        source = self.view.content_source(content)
        path = self.view.shortest_path(receiver, source)
        # Route requests to original source
        self.controller.start_session(time, receiver, content, log)
        self.controller.forward_request_path(receiver, source)
        self.controller.get_content(source)
        # Route content back to receiver
        path = list(reversed(path))
        self.controller.forward_content_path(source, receiver, path)
        self.controller.end_session()


@register_strategy('EDGE')
class Edge(Strategy):
    """Edge caching strategy.

    In this strategy only a cache at the edge is looked up before forwarding
    a content request to the original source.

    In practice, this is like an LCE but it only queries the first cache it
    finds in the path. It is assumed to be used with a topology where each
    PoP has a cache but it simulates a case where the cache is actually further
    down the access network and it is not looked up for transit traffic passing
    through the PoP but only for PoP-originated requests.
    """

    @inheritdoc(Strategy)
    def __init__(self, view, controller):
        super(Edge, self).__init__(view, controller)

    @inheritdoc(Strategy)
    def process_event(self, time, receiver, content, log):
        # get all required data
        source = self.view.content_source(content)
        path = self.view.shortest_path(receiver, source)
        # Route requests to original source and queries caches on the path
        self.controller.start_session(time, receiver, content, log)
        edge_cache = None
        for u, v in path_links(path):
            self.controller.forward_request_hop(u, v)
            if self.view.has_cache(v):
                edge_cache = v
                if self.controller.get_content(v):
                    serving_node = v
                else:
                    # Cache miss, get content from source
                    self.controller.forward_request_path(v, source)
                    self.controller.get_content(source)
                    serving_node = source
                break
        else:
            # No caches on the path at all, get it from source
            self.controller.get_content(v)
            serving_node = v

        # Return content
        path = list(reversed(self.view.shortest_path(receiver, serving_node)))
        self.controller.forward_content_path(serving_node, receiver, path)
        if serving_node == source:
            self.controller.put_content(edge_cache)
        self.controller.end_session()


@register_strategy('RAND_ROUTING')
class RandomSearchAlgorithm(Strategy):

    @inheritdoc(Strategy)
    def __init__(self, view, controller, **kwargs):
        super(LeaveCopyDown, self).__init__(view, controller)
        self.alpha = kwargs['alpha']
	self.network_cache = kwargs['network_cache']
	self.count=1
	self.s_by_cache=0
	self.node_content_presence_table= np.zeros((40,1000,1))
	self.node_breadcrumb_table= np.zeros((40,1000,1))
	self.node_connection_matrix= np.zeros((40,40))
	for i in range(0,40):
            for j in range (0,40):
                if i==j:
                   self.node_connection_matrix[i][j]=1
        self.node_connection_matrix[0][1]=1
        self.node_connection_matrix[0][2]=1
        self.node_connection_matrix[0][4]=1
        self.node_connection_matrix[0][34]=1
        self.node_connection_matrix[0][30]=1
        self.node_connection_matrix[1][33]=1
        self.node_connection_matrix[2][32]=1
        self.node_connection_matrix[2][35]=1
        self.node_connection_matrix[2][4]=1
        self.node_connection_matrix[2][38]=1
        self.node_connection_matrix[2][36]=1
        self.node_connection_matrix[2][31]=1
        self.node_connection_matrix[3][10]=1
        self.node_connection_matrix[3][19]=1
        self.node_connection_matrix[3][4]=1
        self.node_connection_matrix[3][5]=1
        self.node_connection_matrix[3][30]=1
        self.node_connection_matrix[4][5]=1
        self.node_connection_matrix[4][6]=1
        self.node_connection_matrix[4][8]=1
        self.node_connection_matrix[4][16]=1
        self.node_connection_matrix[4][17]=1
        self.node_connection_matrix[4][29]=1
        self.node_connection_matrix[4][31]=1
        self.node_connection_matrix[5][23]=1
        self.node_connection_matrix[7][6]=1
        self.node_connection_matrix[7][8]=1
        self.node_connection_matrix[7][25]=1
        self.node_connection_matrix[7][34]=1
        self.node_connection_matrix[9][8]=1
        self.node_connection_matrix[25][8]=1
        self.node_connection_matrix[9][18]=1
        self.node_connection_matrix[9][25]=1
        self.node_connection_matrix[9][29]=1
        self.node_connection_matrix[9][15]=1
        self.node_connection_matrix[11][13]=1
        self.node_connection_matrix[12][14]=1
        self.node_connection_matrix[12][20]=1
        self.node_connection_matrix[12][13]=1
        self.node_connection_matrix[12][22]=1
        self.node_connection_matrix[12][15]=1
        self.node_connection_matrix[13][22]=1
        self.node_connection_matrix[13][14]=1
        self.node_connection_matrix[15][29]=1
        self.node_connection_matrix[16][34]=1
        self.node_connection_matrix[17][30]=1
        self.node_connection_matrix[21][27]=1
        self.node_connection_matrix[22][26]=1
        self.node_connection_matrix[22][27]=1
        self.node_connection_matrix[22][23]=1
        self.node_connection_matrix[23][29]=1
        self.node_connection_matrix[24][25]=1
        self.node_connection_matrix[24][34]=1
        self.node_connection_matrix[28][27]=1
        self.node_connection_matrix[28][29]=1
        self.node_connection_matrix[30][39]=1
        self.node_connection_matrix[32][34]=1
        self.node_connection_matrix[33][34]=1
        self.node_connection_matrix[35][36]=1
        self.node_connection_matrix[36][37]=1
        self.node_connection_matrix[38][39]=1
        for i in range(0,40):
            for j in range(0,40):
                if self.node_connection_matrix[i][j]==1:
                    self.node_connection_matrix[j][i]=1


    @inheritdoc(Strategy)
    def process_event(self, time, receiver, content, log):
        #file naming
        alpha=self.alpha
        name_alpha = str(alpha)
	cache_size = 1000 * self.network_cache
	name_cache = str(cache_size)
	name_file=file_name("rand_routing_served_by_nodes",name_alpha,name_cache)
	#name_file3=file_name("lcd_content_population",name_alpha,name_cache)
        # get all required data
        n=1000
        source = self.view.content_source(content)
        path = self.view.shortest_path(receiver, source)
        # Route requests to original source and queries caches on the path
        self.controller.start_session(time, receiver, content, log)
        if self.count<=300000:
            for u, v in path_links(path):
                self.controller.forward_request_hop(u, v)
                if self.view.has_cache(v):
                    if self.controller.get_content(v):
                        serving_node = v
                        break
            else:
                # No cache hits, get content from source
                self.controller.get_content(v)
                serving_node = v
        else:
            if self.count==300001:
                for content in range(1,n):
                    caches=[]
                    caches=self.view.content_locations(content)
                    for cache in caches:
                        if int(cache)<1000:
                            self.node_content_presence_table[cache][content][0]=1
            hopCount=0
            nextNode=receiver
            serving_node= source
            while (hopCount<30):
                if self.node_content_presence_table[nextNode][content][0]==1:
                    #serve content from this node, write code
                    serving_node=nextNode
                    break
                elif self.node_breadcrumb_table[nextNode][content][0]!=0:
                    #forward request to this node
                    #update breadcrumb table
                    nNode= self.node_breadcrumb_table[nextNode][content][0]
                    nextNode=nNode
                else:
                    #choose one router randomly from the set of routers with which the receiver is directly connected
                    nextNode=chooseRandNode(self.node_connection_matrix, receiver)
                    if nextNode==2000:
                        break
                hopCount+=1
        # Return content
        path = list(reversed(self.view.shortest_path(receiver, serving_node)))
        if self.count>300000:
            if serving_node<1000:
                pathLen= len(path)
                if pathLen>=2:
                    #write code to update breadcrumb table
                    for i in range(0,pathLen):
                        for j in range(i+1,pathLen):
                            self.node_breadcrumb_table[i][content][0]=j
        # Leave a copy of the content only in the cache one level down the hit
        # caching node
        copied = False
        for u, v in path_links(path):
            self.controller.forward_content_hop(u, v)
            if not copied and v != receiver and self.view.has_cache(v):
                if self.count<300000:
                    self.controller.put_content(v)
                    copied=True
        self.controller.end_session()
        self.count+=1



@register_strategy('LCE')
class LeaveCopyEverywhere(Strategy):
    """Leave Copy Everywhere (LCE) strategy.

    In this strategy a copy of a content is replicated at any cache on the
    path between serving node and receiver.
    """

    @inheritdoc(Strategy)
    def __init__(self, view, controller, **kwargs):
        super(LeaveCopyEverywhere, self).__init__(view, controller)
        #self.alpha = kwargs['alpha']
	#self.network_cache = kwargs['network_cache']
	self.count=1
	self.s_by_cache=0
	self.s_by_potential=0
	self.s_by_breadcrumb=0
	self.s_by_random=0
	#the rest of the part can be deleted later, it's for my routing strategy
	#self.node_content_presence_table= np.zeros((40,1000,1))
	self.node_content_presence_table=[[[0 for x in range(1)] for y in range(100000)] for z in range(40)]
	#self.node_breadcrumb_table= np.zeros((40,1000,1))
	self.node_breadcrumb_table=[[[-1 for x in range(1)] for y in range(100000)] for z in range(40)]
	#self.node_connection_matrix= np.zeros((40,40))
	self.node_connection_matrix=[[0 for x in range(40)] for y in range(40)]
	for i in range(0,40):
            for j in range (0,40):
                if i==j:
                   self.node_connection_matrix[i][j]=-1
        self.node_connection_matrix[0][1]=1
        self.node_connection_matrix[0][2]=1
        self.node_connection_matrix[0][4]=1
        self.node_connection_matrix[0][34]=1
        self.node_connection_matrix[0][30]=1
        self.node_connection_matrix[1][33]=1
        self.node_connection_matrix[2][32]=1
        self.node_connection_matrix[2][35]=1
        self.node_connection_matrix[2][4]=1
        self.node_connection_matrix[2][38]=1
        self.node_connection_matrix[2][36]=1
        self.node_connection_matrix[2][31]=1
        self.node_connection_matrix[3][10]=1
        self.node_connection_matrix[3][19]=1
        self.node_connection_matrix[3][4]=1
        self.node_connection_matrix[3][5]=1
        self.node_connection_matrix[3][30]=1
        self.node_connection_matrix[4][5]=1
        self.node_connection_matrix[4][6]=1
        self.node_connection_matrix[4][8]=1
        self.node_connection_matrix[4][16]=1
        self.node_connection_matrix[4][17]=1
        self.node_connection_matrix[4][29]=1
        self.node_connection_matrix[4][31]=1
        self.node_connection_matrix[5][23]=1
        self.node_connection_matrix[7][6]=1
        self.node_connection_matrix[7][8]=1
        self.node_connection_matrix[7][25]=1
        self.node_connection_matrix[7][34]=1
        self.node_connection_matrix[9][8]=1
        self.node_connection_matrix[25][8]=1
        self.node_connection_matrix[9][18]=1
        self.node_connection_matrix[9][25]=1
        self.node_connection_matrix[9][29]=1
        self.node_connection_matrix[9][15]=1
        self.node_connection_matrix[11][13]=1
        self.node_connection_matrix[12][14]=1
        self.node_connection_matrix[12][20]=1
        self.node_connection_matrix[12][13]=1
        self.node_connection_matrix[12][22]=1
        self.node_connection_matrix[12][15]=1
        self.node_connection_matrix[13][22]=1
        self.node_connection_matrix[13][14]=1
        self.node_connection_matrix[15][29]=1
        self.node_connection_matrix[16][34]=1
        self.node_connection_matrix[17][30]=1
        self.node_connection_matrix[21][27]=1
        self.node_connection_matrix[22][26]=1
        self.node_connection_matrix[22][27]=1
        self.node_connection_matrix[22][23]=1
        self.node_connection_matrix[23][29]=1
        self.node_connection_matrix[24][25]=1
        self.node_connection_matrix[24][34]=1
        self.node_connection_matrix[28][27]=1
        self.node_connection_matrix[28][29]=1
        self.node_connection_matrix[30][39]=1
        self.node_connection_matrix[32][34]=1
        self.node_connection_matrix[33][34]=1
        self.node_connection_matrix[35][36]=1
        self.node_connection_matrix[36][37]=1
        self.node_connection_matrix[38][39]=1
        for i in range(0,40):
            for j in range(0,40):
                if self.node_connection_matrix[i][j]==1:
                    self.node_connection_matrix[j][i]=1


    @inheritdoc(Strategy)
    def process_event(self, time, receiver, content, log):
        #file naming
        """alpha=self.alpha
        name_alpha = str(alpha)
	cache_size = 1000 * self.network_cache
	name_cache = str(cache_size)
	name_file=file_name("lce_served_by_nodes_server_up",name_alpha,name_cache)"""
	#name_file3=file_name("lce_content_population",name_alpha,name_cache)
        # get all required data
	n =100000
	if self.count%100000==0:
            print self.count
        source = self.view.content_source(content)
        """testing server content"""
        """if self.count == 2:
            source_list = list(self.view.topology().sources())
            #print source_list
            src_cnt = [0]*13
            for j in range(1,n+1):
                k = self.view.content_source(j)
                i = source_list.index(k)
                src_cnt[i] += 1
            print src_cnt"""
        path = self.view.shortest_path(receiver, source)
        self.controller.start_session(time, receiver, content, log)
        if self.count<=2000000:
            for u, v in path_links(path):
                self.controller.forward_request_hop(u, v)
                if self.view.has_cache(v):
                    if self.controller.get_content(v):
                        serving_node = v
                        break
                self.controller.get_content(v)
                serving_node = v
            path = list(reversed(self.view.shortest_path(receiver, serving_node)))
            for u, v in path_links(path):
                self.controller.forward_content_hop(u, v)
                if self.view.has_cache(v):
                    # insert content
                    self.controller.put_content(v)
        else:
            #fo1.write('\ncount is: %d\n'%self.count)
            #fo1.write('source of content is: %d\n'%source)
            if self.count==2000001:
                #fo1.write('finalizing caches..')
                for content in range(1,n+1):
                    #fo1.write('content id is: %d\n'%content)
                    #print "content is: ",content
                    caches=[]
                    caches=self.view.content_locations(content)
                    #fo1.write('sources are: %s'%caches)
                    #print "caches for this content are: ",caches
                    for cache in caches:
                        if int(cache)<1000:
                            #print "cache is: ",cache
                            #print "content is: ",content
                            self.node_content_presence_table[cache][content-1][0]=1
                            #print "content added to this cache..\n\n"

            #fo1.write('\n\n\n\n')
            hopCount=0
            nextNode=receiver
            serving_node= source
            #sFlag= False
            #rFlag= False
            while hopCount<6:
                hopCount=hopCount+1
                #print "entering in hopcount.."
                #print "next node is: ",nextNode
                #fo1.write('searching in node: %d\n'%nextNode)
                #print "content id is: ",content
                #print "hop count is: ",hopCount
                #fo1.write('hop count is: %d\n'%hopCount)
                #print "element in node content presence table is: "
                #print self.node_content_presence_table[nextNode][content][0]
                if self.node_content_presence_table[nextNode][content-1][0]==1:
                    #serve content from this node, write code
                    #print "entered in first block.."
                    serving_node=nextNode
                    #if self.count>149000:
                        #fo1.write('1st block:serving node of content is: %d\n'%serving_node)
                    break
                elif self.node_breadcrumb_table[nextNode][content-1][0]>-1:
                    #forward request to this node
                    #update breadcrumb table
                    #print "entered in second block.."
                    nNode= self.node_breadcrumb_table[nextNode][content-1][0]
                    #sFlag= True
                    #if self.count>149000:
                        #fo1.write('2nd block:content entry found from breadcrumb table. next node is: %d\n'%nNode)
                    nextNode=nNode
                else:
                    #choose one router randomly from the set of routers with which the receiver is directly connected
                    #print "entered in last block.."
                    #nextNode=chooseRandNode(self.node_connection_matrix, receiver)
                    #fo1.write('content not found in cache or breadcrumb table\n')
                    conSet=[]
                    #rFlag= True
                    matLen=len(self.node_connection_matrix[0])
                    #print "length is: ",matLen
                    for i in range(0,40):
                        if self.node_connection_matrix[nextNode][i]==1:
                            conSet.append(i)
                            #print "appended i value is: ",i
                    conSetLen=len(conSet)
                    #print "conSetLen is: ",conSetLen
                    if conSetLen==0:
                        #print "no element in conSet.."
                        nextNode=2000
                    elif conSetLen==1:
                        #print "only 1 element in conset. that is: ",conSet[0]
                        nextNode=conSet[0]
                    else:
                        #print "more than 1 element in conSet.."
                        r=random.randint(1,conSetLen)
                        nextNode=conSet[r-1]
                    #if self.count>149000:
                        #fo1.write('3rd block:next node has been chosen randomly. hop count is: %d\n'%hopCount)
                    #print "next node has been chosen randomly. next node is: ",nextNode
                    if nextNode==2000:
                        #fo1.write('next node is 2000. terminating\n')
                        break
            #print "hopcount section over.."
            #fo1.write('hop count section over...\n\n\n')
            # Return content
            path = list(reversed(self.view.shortest_path(receiver, serving_node)))
            #calculation of request serving potential
            if self.count>2000000:
                caches=[]
                caches=self.view.content_locations(content)
                for cache in caches:
                    if int(cache)<1000:
                        self.s_by_potential+=1
                        break
            #path1=path.reverse()
            #print "content serving path is: ",path
            #print "serving node is: ",serving_node
            #print "requester is: ",receiver
            if serving_node<1000:
                self.s_by_cache+=1
                pathLen= len(path)
                if pathLen>=2:
                    #write code to update breadcrumb table
                    i=0
                    j=1
                    while j<pathLen:
                        if path[i]<1000 and path[j]<1000:
                            self.node_breadcrumb_table[path[j]][content-1][0]=path[i]
                            #print "updating breadcrumb table.."
                            #print "for %d"%path[j]
                            #print " value is %d"%path[i]
                        i+=1
                        j+=1
                #if self.count>100000:
                    #self.s_by_cache+=1
        #fo1= open('%s.txt'%name_file1,'a')
        #fo1.write('%d\n'%self.s_by_cache)
        #fo1.close()
        #fo1.write('breadcrumb history:\n\n')
        #if self.count>149000:
            #if sFlag==True and serving_node<1000:
                #self.s_by_breadcrumb+=1
            #if rFlag==True and serving_node<1000:
                #self.s_by_random+=1
        #sFlag= False
        #rFlag= False
        if self.count>2000000:
            if self.count%100000==0:
                fo= open('my_algo_lce_content_100K.txt','a')
                fo.write('served_my algo: %d'%self.s_by_cache)
                fo.write('    potential: %d\n'%self.s_by_potential)
                fo.close()
                self.s_by_cache=0
                self.s_by_potential=0
            #fo1.write('\n\n\n\nserved by breadcrumb value is: %d\n'%self.s_by_breadcrumb)
            #fo1.write('served by random value is: %d\n'%self.s_by_random)
            #fo1.write('total requests service potential: %d\n'%self.s_by_potential)
            #fo1.write('total requests served by caches: %d'%self.s_by_cache)
        self.count+=1
        #fo1.close()
        self.controller.end_session()











@register_strategy('LCD')
class LeaveCopyDown(Strategy):
    """Leave Copy Down (LCD) strategy.

    According to this strategy, one copy of a content is replicated only in
    the caching node you hop away from the serving node in the direction of
    the receiver. This strategy is described in [2]_.

    Rereferences
    ------------
    ..[2] N. Laoutaris, H. Che, i. Stavrakakis, The LCD interconnection of LRU
          caches and its analysis.
          Available: http://cs-people.bu.edu/nlaout/analysis_PEVA.pdf
    """

    @inheritdoc(Strategy)
    def __init__(self, view, controller, **kwargs):
        super(LeaveCopyDown, self).__init__(view, controller)
        #self.alpha = kwargs['alpha']
	#self.network_cache = kwargs['network_cache']
	self.count=1
	self.s_by_cache=0
	self.s_by_potential=0
	self.s_by_breadcrumb=0
	self.s_by_random=0
	#the rest of the part can be deleted later, it's for my routing strategy
	#self.node_content_presence_table= np.zeros((40,1000,1))
	self.node_content_presence_table=[[[0 for x in range(1)] for y in range(100000)] for z in range(61)]
	#self.node_breadcrumb_table= np.zeros((40,1000,1))
	self.node_breadcrumb_table=[[[-1 for x in range(1)] for y in range(100000)] for z in range(61)]
	#self.node_connection_matrix= np.zeros((40,40))
	self.node_connection_matrix=[[0 for x in range(61)] for y in range(61)]
	"""with open('node_connection_mat','r') as dest_f:
            data_iter = csv.reader(dest_f,
                           delimiter = delimiter,
                           quotechar = '"')
            data = [data for data in data_iter]
        self.node_connection_matrix = np.asarray(data)"""
	for i in range(0,61):
            for j in range (0,61):
                if i==j:
                   self.node_connection_matrix[i][j]=-1
        self.node_connection_matrix[1][4]=1
        self.node_connection_matrix[2][35]=1
        self.node_connection_matrix[3][37]=1
        self.node_connection_matrix[4][10]=1
        self.node_connection_matrix[4][55]=1
        self.node_connection_matrix[4][7]=1
        self.node_connection_matrix[5][55]=1
        self.node_connection_matrix[6][45]=1
        self.node_connection_matrix[6][55]=1
        self.node_connection_matrix[8][22]=1
        self.node_connection_matrix[9][58]=1
        self.node_connection_matrix[10][44]=1
        self.node_connection_matrix[10][45]=1
        self.node_connection_matrix[10][21]=1
        self.node_connection_matrix[10][55]=1
        self.node_connection_matrix[11][55]=1
        self.node_connection_matrix[12][20]=1
        self.node_connection_matrix[13][29]=1
        self.node_connection_matrix[14][34]=1
        self.node_connection_matrix[14][35]=1
        self.node_connection_matrix[14][46]=1
        self.node_connection_matrix[14][49]=1
        self.node_connection_matrix[14][55]=1
        self.node_connection_matrix[14][57]=1
        self.node_connection_matrix[14][26]=1
        self.node_connection_matrix[14][29]=1
        self.node_connection_matrix[15][35]=1
        self.node_connection_matrix[15][53]=1
        self.node_connection_matrix[15][37]=1
        self.node_connection_matrix[16][34]=1
        self.node_connection_matrix[17][56]=1
        self.node_connection_matrix[17][34]=1
        self.node_connection_matrix[18][41]=1
        self.node_connection_matrix[18][43]=1
        self.node_connection_matrix[18][21]=1
        self.node_connection_matrix[18][55]=1
        self.node_connection_matrix[18][49]=1
        self.node_connection_matrix[19][21]=1
        self.node_connection_matrix[20][21]=1
        self.node_connection_matrix[21][22]=1
        self.node_connection_matrix[21][23]=1
        self.node_connection_matrix[24][37]=1
        self.node_connection_matrix[25][37]=1
        self.node_connection_matrix[27][55]=1
        self.node_connection_matrix[28][55]=1
        self.node_connection_matrix[29][30]=1
        self.node_connection_matrix[24][37]=1
        self.node_connection_matrix[29][59]=1
        self.node_connection_matrix[31][33]=1
        self.node_connection_matrix[31][34]=1
        self.node_connection_matrix[32][49]=1
        self.node_connection_matrix[34][35]=1
        self.node_connection_matrix[34][36]=1
        self.node_connection_matrix[35][37]=1
        self.node_connection_matrix[35][52]=1
        self.node_connection_matrix[35][56]=1
        self.node_connection_matrix[36][37]=1
        self.node_connection_matrix[51][37]=1
        self.node_connection_matrix[38][37]=1
        self.node_connection_matrix[39][37]=1
        self.node_connection_matrix[40][37]=1
        self.node_connection_matrix[56][37]=1
        self.node_connection_matrix[55][37]=1
        self.node_connection_matrix[58][37]=1
        self.node_connection_matrix[38][56]=1
        self.node_connection_matrix[39][59]=1
        self.node_connection_matrix[40][59]=1
        self.node_connection_matrix[40][54]=1
        self.node_connection_matrix[42][49]=1
        self.node_connection_matrix[44][55]=1
        self.node_connection_matrix[46][60]=1
        self.node_connection_matrix[47][49]=1
        self.node_connection_matrix[48][49]=1
        self.node_connection_matrix[49][50]=1
        self.node_connection_matrix[55][59]=1
        for i in range(0,61):
            for j in range(0,61):
                if self.node_connection_matrix[i][j]==1:
                    self.node_connection_matrix[j][i]=1

    @inheritdoc(Strategy)
    def process_event(self, time, receiver, content, log):
        #file naming
	#name_file3=file_name("lcd_content_population",name_alpha,name_cache)
        # get all required data
        n=100000
        source = self.view.content_source(content)
        if self.count%100000==0:
            print self.count
        #print "source of the content is: ",source
        path = self.view.shortest_path(receiver, source)
        # Route requests to original source and queries caches on the path
        self.controller.start_session(time, receiver, content, log)
        if self.count<=2000000:
            for u, v in path_links(path):
                self.controller.forward_request_hop(u, v)
                if self.view.has_cache(v):
                    if self.controller.get_content(v):
                        serving_node = v
                        break
            else:
            # No cache hits, get content from source
                self.controller.get_content(v)
                serving_node = v
            # Return content
            path = list(reversed(self.view.shortest_path(receiver, serving_node)))
            # Leave a copy of the content only in the cache one level down the hit
            # caching node
            copied = False
            for u, v in path_links(path):
                self.controller.forward_content_hop(u, v)
                if not copied and v != receiver and self.view.has_cache(v):
                    self.controller.put_content(v)
                    copied = True
        else:
            #fo1.write('\ncount is: %d\n'%self.count)
            #fo1.write('source of content is: %d\n'%source)
            if self.count==2000001:
                #fo1.write('finalizing caches..')
                for content in range(1,n+1):
                    #fo1.write('content id is: %d\n'%content)
                    #print "content is: ",content
                    caches=[]
                    caches=self.view.content_locations(content)
                    #fo1.write('sources are: %s'%caches)
                    #print "caches for this content are: ",caches
                    for cache in caches:
                        if cache is not source:
                            #print "cache is: ",cache
                            #print "content is: ",content
                            self.node_content_presence_table[cache][content-1][0]=1
                            #print "content added to this cache..\n\n"

            #fo1.write('\n\n\n\n')
            hopCount=0
            nextNode=receiver
            serving_node= source
            #sFlag= False
            #rFlag= False
            while hopCount<9:
                hopCount=hopCount+1
                #print "entering in hopcount.."
                #print "next node is: ",nextNode
                #fo1.write('searching in node: %d\n'%nextNode)
                #print "content id is: ",content
                #print "hop count is: ",hopCount
                #fo1.write('hop count is: %d\n'%hopCount)
                #print "element in node content presence table is: "
                #print self.node_content_presence_table[nextNode][content][0]
                if self.node_content_presence_table[nextNode][content-1][0]==1:
                    #serve content from this node, write code
                    #print "entered in first block.."
                    serving_node=nextNode
                    #if self.count>149000:
                        #fo1.write('1st block:serving node of content is: %d\n'%serving_node)
                    break
                elif self.node_breadcrumb_table[nextNode][content-1][0]>-1:
                    #forward request to this node
                    #update breadcrumb table
                    #print "entered in second block.."
                    nNode= self.node_breadcrumb_table[nextNode][content-1][0]
                    #sFlag= True
                    #if self.count>149000:
                        #fo1.write('2nd block:content entry found from breadcrumb table. next node is: %d\n'%nNode)
                    nextNode=nNode
                else:
                    #choose one router randomly from the set of routers with which the receiver is directly connected
                    #print "entered in last block.."
                    #nextNode=chooseRandNode(self.node_connection_matrix, receiver)
                    #fo1.write('content not found in cache or breadcrumb table\n')
                    conSet=[]
                    #rFlag= True
                    matLen=len(self.node_connection_matrix[0])
                    #print "length is: ",matLen
                    for i in range(0, 61):
                        if self.node_connection_matrix[nextNode][i]==1:
                            conSet.append(i)
                            #print "appended i value is: ",i
                    conSetLen=len(conSet)
                    #print "conSetLen is: ",conSetLen
                    if conSetLen==0:
                        #print "no element in conSet.."
                        nextNode=2000
                    elif conSetLen==1:
                        #print "only 1 element in conset. that is: ",conSet[0]
                        nextNode=conSet[0]
                    else:
                        #print "more than 1 element in conSet.."
                        r=random.randint(1,conSetLen)
                        nextNode=conSet[r-1]
                    #if self.count>149000:
                        #fo1.write('3rd block:next node has been chosen randomly. hop count is: %d\n'%hopCount)
                    #print "next node has been chosen randomly. next node is: ",nextNode
                    if nextNode==2000:
                        #fo1.write('next node is 2000. terminating\n')
                        break
            #print "hopcount section over.."
            #fo1.write('hop count section over...\n\n\n')
            # Return content
            path = list(reversed(self.view.shortest_path(receiver, serving_node)))
            #calculation of request serving potential
            if self.count>2000000:
                caches=[]
                caches=self.view.content_locations(content)
                for cache in caches:
                    if cache is not source:
                        self.s_by_potential+=1
                        break
            #path1=path.reverse()
            #print "content serving path is: ",path
            #print "serving node is: ",serving_node
            #print "requester is: ",receiver
            if serving_node is not source:
                self.s_by_cache+=1
                pathLen= len(path)
                if pathLen>=2:
                    #write code to update breadcrumb table
                    i=0
                    j=1
                    while j<pathLen:
                        if (path[i] is not source) and (path[j] is not source):
                            self.node_breadcrumb_table[path[j]][content-1][0]=path[i]
                            #print "updating breadcrumb table.."
                            #print "for %d"%path[j]
                            #print " value is %d"%path[i]
                        i+=1
                        j+=1
                #if self.count>100000:
                    #self.s_by_cache+=1
        #fo1= open('%s.txt'%name_file1,'a')
        #fo1.write('%d\n'%self.s_by_cache)
        #fo1.close()
        #fo1.write('breadcrumb history:\n\n')
        #if self.count>149000:
            #if sFlag==True and serving_node<1000:
                #self.s_by_breadcrumb+=1
            #if rFlag==True and serving_node<1000:
                #self.s_by_random+=1
        #sFlag= False
        #rFlag= False
        if self.count>2000000:
            if self.count%300000==0:
                fo= open('my_algo_steady_state.txt','a')
                #fo.write('alpha: '%self.alpha)
                fo.write('served_my algo: %d'%self.s_by_cache)
                fo.write('    potential: %d\n'%self.s_by_potential)
                fo.close()
                self.s_by_cache=0
                self.s_by_potential=0
            #fo1.write('\n\n\n\nserved by breadcrumb value is: %d\n'%self.s_by_breadcrumb)
            #fo1.write('served by random value is: %d\n'%self.s_by_random)
            #fo1.write('total requests service potential: %d\n'%self.s_by_potential)
            #fo1.write('total requests served by caches: %d'%self.s_by_cache)
        self.count+=1
        #fo1.close()
        self.controller.end_session()



@register_strategy('PROB_CACHE')
class ProbCache(Strategy):
    """ProbCache strategy [4]_

    This strategy caches content objects probabilistically on a path with a
    probability depending on various factors, including distance from source
    and destination and caching space available on the path.

    This strategy was originally proposed in [3]_ and extended in [4]_. This
    class implements the extended version described in [4]_. In the extended
    version of ProbCache the :math`x/c` factor of the ProbCache equation is
    raised to the power of :math`c`.

    References
    ----------
    ..[3] I. Psaras, W. Chai, G. Pavlou, Probabilistic In-Network Caching for
          Information-Centric Networks, in Proc. of ACM SIGCOMM ICN '12
          Available: http://www.ee.ucl.ac.uk/~uceeips/prob-cache-icn-sigcomm12.pdf
    ..[4] I. Psaras, W. Chai, G. Pavlou, In-Network Cache Management and
          Resource Allocation for Information-Centric Networks, IEEE
          Transactions on Parallel and Distributed Systems, 22 May 2014
          Available: http://doi.ieeecomputersociety.org/10.1109/TPDS.2013.304
    """

    @inheritdoc(Strategy)
    def __init__(self, view, controller, t_tw=10):
        super(ProbCache, self).__init__(view, controller)
        self.t_tw = t_tw
        self.cache_size = view.cache_nodes(size=True)
        self.count=1
	self.s_by_cache=0
	self.s_by_potential=0
	self.s_by_breadcrumb=0
	self.s_by_random=0
	#the rest of the part can be deleted later, it's for my routing strategy
	#self.node_content_presence_table= np.zeros((40,1000,1))
	self.node_content_presence_table=[[[0 for x in range(1)] for y in range(100000)] for z in range(40)]
	#self.node_breadcrumb_table= np.zeros((40,1000,1))
	self.node_breadcrumb_table=[[[-1 for x in range(1)] for y in range(100000)] for z in range(40)]
	#self.node_connection_matrix= np.zeros((40,40))
	self.node_connection_matrix=[[0 for x in range(40)] for y in range(40)]
	for i in range(0,40):
            for j in range (0,40):
                if i==j:
                   self.node_connection_matrix[i][j]=-1
        self.node_connection_matrix[0][1]=1
        self.node_connection_matrix[0][2]=1
        self.node_connection_matrix[0][4]=1
        self.node_connection_matrix[0][34]=1
        self.node_connection_matrix[0][30]=1
        self.node_connection_matrix[1][33]=1
        self.node_connection_matrix[2][32]=1
        self.node_connection_matrix[2][35]=1
        self.node_connection_matrix[2][4]=1
        self.node_connection_matrix[2][38]=1
        self.node_connection_matrix[2][36]=1
        self.node_connection_matrix[2][31]=1
        self.node_connection_matrix[3][10]=1
        self.node_connection_matrix[3][19]=1
        self.node_connection_matrix[3][4]=1
        self.node_connection_matrix[3][5]=1
        self.node_connection_matrix[3][30]=1
        self.node_connection_matrix[4][5]=1
        self.node_connection_matrix[4][6]=1
        self.node_connection_matrix[4][8]=1
        self.node_connection_matrix[4][16]=1
        self.node_connection_matrix[4][17]=1
        self.node_connection_matrix[4][29]=1
        self.node_connection_matrix[4][31]=1
        self.node_connection_matrix[5][23]=1
        self.node_connection_matrix[7][6]=1
        self.node_connection_matrix[7][8]=1
        self.node_connection_matrix[7][25]=1
        self.node_connection_matrix[7][34]=1
        self.node_connection_matrix[9][8]=1
        self.node_connection_matrix[25][8]=1
        self.node_connection_matrix[9][18]=1
        self.node_connection_matrix[9][25]=1
        self.node_connection_matrix[9][29]=1
        self.node_connection_matrix[9][15]=1
        self.node_connection_matrix[11][13]=1
        self.node_connection_matrix[12][14]=1
        self.node_connection_matrix[12][20]=1
        self.node_connection_matrix[12][13]=1
        self.node_connection_matrix[12][22]=1
        self.node_connection_matrix[12][15]=1
        self.node_connection_matrix[13][22]=1
        self.node_connection_matrix[13][14]=1
        self.node_connection_matrix[15][29]=1
        self.node_connection_matrix[16][34]=1
        self.node_connection_matrix[17][30]=1
        self.node_connection_matrix[21][27]=1
        self.node_connection_matrix[22][26]=1
        self.node_connection_matrix[22][27]=1
        self.node_connection_matrix[22][23]=1
        self.node_connection_matrix[23][29]=1
        self.node_connection_matrix[24][25]=1
        self.node_connection_matrix[24][34]=1
        self.node_connection_matrix[28][27]=1
        self.node_connection_matrix[28][29]=1
        self.node_connection_matrix[30][39]=1
        self.node_connection_matrix[32][34]=1
        self.node_connection_matrix[33][34]=1
        self.node_connection_matrix[35][36]=1
        self.node_connection_matrix[36][37]=1
        self.node_connection_matrix[38][39]=1
        for i in range(0,40):
            for j in range(0,40):
                if self.node_connection_matrix[i][j]==1:
                    self.node_connection_matrix[j][i]=1

    @inheritdoc(Strategy)
    def process_event(self, time, receiver, content, log):
        # get all required data
        n=100000
        source = self.view.content_source(content)
        path = self.view.shortest_path(receiver, source)
        # Route requests to original source and queries caches on the path
        self.controller.start_session(time, receiver, content, log)
        if self.count<=2000000:
            for hop in range(1, len(path)):
                u = path[hop - 1]
                v = path[hop]
                self.controller.forward_request_hop(u, v)
                if self.view.has_cache(v):
                    if self.controller.get_content(v):
                        serving_node = v
                        break
            else:
                # No cache hits, get content from source
                self.controller.get_content(v)
                serving_node = v
            # Return content
            path = list(reversed(self.view.shortest_path(receiver, serving_node)))
            c = len([v for v in path if self.view.has_cache(v)])
            x = 0.0
            for hop in range(1, len(path)):
                u = path[hop - 1]
                v = path[hop]
                N = sum([self.cache_size[n] for n in path[hop - 1:]
                         if n in self.cache_size])
                if v in self.cache_size:
                    x += 1
                self.controller.forward_content_hop(u, v)
                if v != receiver and v in self.cache_size:
                    # The (x/c) factor raised to the power of "c" according to the
                    # extended version of ProbCache published in IEEE TPDS
                    prob_cache = float(N)/(self.t_tw * self.cache_size[v])*(x/c)**c
                    if random.random() < prob_cache:
                        self.controller.put_content(v)
        else:
            #fo1.write('\ncount is: %d\n'%self.count)
            #fo1.write('source of content is: %d\n'%source)
            if self.count==2000001:
                #fo1.write('finalizing caches..')
                for content in range(1,n+1):
                    #fo1.write('content id is: %d\n'%content)
                    #print "content is: ",content
                    caches=[]
                    caches=self.view.content_locations(content)
                    #fo1.write('sources are: %s'%caches)
                    #print "caches for this content are: ",caches
                    for cache in caches:
                        if int(cache)<1000:
                            #print "cache is: ",cache
                            #print "content is: ",content
                            self.node_content_presence_table[cache][content-1][0]=1
                            #print "content added to this cache..\n\n"

            #fo1.write('\n\n\n\n')
            hopCount=0
            nextNode=receiver
            serving_node= source
            #sFlag= False
            #rFlag= False
            while hopCount<6:
                hopCount=hopCount+1
                #print "entering in hopcount.."
                #print "next node is: ",nextNode
                #fo1.write('searching in node: %d\n'%nextNode)
                #print "content id is: ",content
                #print "hop count is: ",hopCount
                #fo1.write('hop count is: %d\n'%hopCount)
                #print "element in node content presence table is: "
                #print self.node_content_presence_table[nextNode][content][0]
                if self.node_content_presence_table[nextNode][content-1][0]==1:
                    #serve content from this node, write code
                    #print "entered in first block.."
                    serving_node=nextNode
                    #if self.count>149000:
                        #fo1.write('1st block:serving node of content is: %d\n'%serving_node)
                    break
                elif self.node_breadcrumb_table[nextNode][content-1][0]>-1:
                    #forward request to this node
                    #update breadcrumb table
                    #print "entered in second block.."
                    nNode= self.node_breadcrumb_table[nextNode][content-1][0]
                    #sFlag= True
                    #if self.count>149000:
                        #fo1.write('2nd block:content entry found from breadcrumb table. next node is: %d\n'%nNode)
                    nextNode=nNode
                else:
                    #choose one router randomly from the set of routers with which the receiver is directly connected
                    #print "entered in last block.."
                    #nextNode=chooseRandNode(self.node_connection_matrix, receiver)
                    #fo1.write('content not found in cache or breadcrumb table\n')
                    conSet=[]
                    #rFlag= True
                    matLen=len(self.node_connection_matrix[0])
                    #print "length is: ",matLen
                    for i in range(0,40):
                        if self.node_connection_matrix[nextNode][i]==1:
                            conSet.append(i)
                            #print "appended i value is: ",i
                    conSetLen=len(conSet)
                    #print "conSetLen is: ",conSetLen
                    if conSetLen==0:
                        #print "no element in conSet.."
                        nextNode=2000
                    elif conSetLen==1:
                        #print "only 1 element in conset. that is: ",conSet[0]
                        nextNode=conSet[0]
                    else:
                        #print "more than 1 element in conSet.."
                        r=random.randint(1,conSetLen)
                        nextNode=conSet[r-1]
                    #if self.count>149000:
                        #fo1.write('3rd block:next node has been chosen randomly. hop count is: %d\n'%hopCount)
                    #print "next node has been chosen randomly. next node is: ",nextNode
                    if nextNode==2000:
                        #fo1.write('next node is 2000. terminating\n')
                        break
            #print "hopcount section over.."
            #fo1.write('hop count section over...\n\n\n')
            # Return content
            path = list(reversed(self.view.shortest_path(receiver, serving_node)))
            #calculation of request serving potential
            if self.count>2000000:
                caches=[]
                caches=self.view.content_locations(content)
                for cache in caches:
                    if int(cache)<1000:
                        self.s_by_potential+=1
                        break
            #path1=path.reverse()
            #print "content serving path is: ",path
            #print "serving node is: ",serving_node
            #print "requester is: ",receiver
            if serving_node<1000:
                self.s_by_cache+=1
                pathLen= len(path)
                if pathLen>=2:
                    #write code to update breadcrumb table
                    i=0
                    j=1
                    while j<pathLen:
                        if path[i]<1000 and path[j]<1000:
                            self.node_breadcrumb_table[path[j]][content-1][0]=path[i]
                            #print "updating breadcrumb table.."
                            #print "for %d"%path[j]
                            #print " value is %d"%path[i]
                        i+=1
                        j+=1
                #if self.count>100000:
                    #self.s_by_cache+=1
        #fo1= open('%s.txt'%name_file1,'a')
        #fo1.write('%d\n'%self.s_by_cache)
        #fo1.close()
        #fo1.write('breadcrumb history:\n\n')
        #if self.count>149000:
            #if sFlag==True and serving_node<1000:
                #self.s_by_breadcrumb+=1
            #if rFlag==True and serving_node<1000:
                #self.s_by_random+=1
        #sFlag= False
        #rFlag= False
        if self.count>2000000:
            if self.count%100000==0:
                fo= open('my_algo_prob_cache_alpha_08.txt','a')
                fo.write('served_my algo: %d'%self.s_by_cache)
                fo.write('    potential: %d\n'%self.s_by_potential)
                fo.close()
                self.s_by_cache=0
                self.s_by_potential=0
            #fo1.write('\n\n\n\nserved by breadcrumb value is: %d\n'%self.s_by_breadcrumb)
            #fo1.write('served by random value is: %d\n'%self.s_by_random)
            #fo1.write('total requests service potential: %d\n'%self.s_by_potential)
            #fo1.write('total requests served by caches: %d'%self.s_by_cache)
        self.count+=1
        #fo1.close()
        self.controller.end_session()


@register_strategy('CL4M')
class CacheLessForMore(Strategy):
    """Cache less for more strategy [5]_.

    References
    ----------
    ..[5] W. Chai, D. He, I. Psaras, G. Pavlou, Cache Less for More in
          Information-centric Networks, in IFIP NETWORKING '12
          Available: http://www.ee.ucl.ac.uk/~uceeips/centrality-networking12.pdf
    """

    @inheritdoc(Strategy)
    def __init__(self, view, controller, use_ego_betw=False, **kwargs):
        super(CacheLessForMore, self).__init__(view, controller)
        #self.alpha = kwargs['alpha']
	#self.network_cache = kwargs['network_cache']
	self.count=1
	self.s_by_cache=0
        topology = view.topology()
        if use_ego_betw:
            self.betw = dict((v, nx.betweenness_centrality(nx.ego_graph(topology, v))[v])
                             for v in topology.nodes_iter())
        else:
            self.betw = nx.betweenness_centrality(topology)
	self.s_by_potential=0
	self.s_by_breadcrumb=0
	self.s_by_random=0
	#the rest of the part can be deleted later, it's for my routing strategy
	#self.node_content_presence_table= np.zeros((40,1000,1))
	self.node_content_presence_table=[[[0 for x in range(1)] for y in range(100000)] for z in range(40)]
	#self.node_breadcrumb_table= np.zeros((40,1000,1))
	self.node_breadcrumb_table=[[[-1 for x in range(1)] for y in range(100000)] for z in range(40)]
	#self.node_connection_matrix= np.zeros((40,40))
	self.node_connection_matrix=[[0 for x in range(40)] for y in range(40)]
	for i in range(0,40):
            for j in range (0,40):
                if i==j:
                   self.node_connection_matrix[i][j]=-1
        self.node_connection_matrix[0][1]=1
        self.node_connection_matrix[0][2]=1
        self.node_connection_matrix[0][4]=1
        self.node_connection_matrix[0][34]=1
        self.node_connection_matrix[0][30]=1
        self.node_connection_matrix[1][33]=1
        self.node_connection_matrix[2][32]=1
        self.node_connection_matrix[2][35]=1
        self.node_connection_matrix[2][4]=1
        self.node_connection_matrix[2][38]=1
        self.node_connection_matrix[2][36]=1
        self.node_connection_matrix[2][31]=1
        self.node_connection_matrix[3][10]=1
        self.node_connection_matrix[3][19]=1
        self.node_connection_matrix[3][4]=1
        self.node_connection_matrix[3][5]=1
        self.node_connection_matrix[3][30]=1
        self.node_connection_matrix[4][5]=1
        self.node_connection_matrix[4][6]=1
        self.node_connection_matrix[4][8]=1
        self.node_connection_matrix[4][16]=1
        self.node_connection_matrix[4][17]=1
        self.node_connection_matrix[4][29]=1
        self.node_connection_matrix[4][31]=1
        self.node_connection_matrix[5][23]=1
        self.node_connection_matrix[7][6]=1
        self.node_connection_matrix[7][8]=1
        self.node_connection_matrix[7][25]=1
        self.node_connection_matrix[7][34]=1
        self.node_connection_matrix[9][8]=1
        self.node_connection_matrix[25][8]=1
        self.node_connection_matrix[9][18]=1
        self.node_connection_matrix[9][25]=1
        self.node_connection_matrix[9][29]=1
        self.node_connection_matrix[9][15]=1
        self.node_connection_matrix[11][13]=1
        self.node_connection_matrix[12][14]=1
        self.node_connection_matrix[12][20]=1
        self.node_connection_matrix[12][13]=1
        self.node_connection_matrix[12][22]=1
        self.node_connection_matrix[12][15]=1
        self.node_connection_matrix[13][22]=1
        self.node_connection_matrix[13][14]=1
        self.node_connection_matrix[15][29]=1
        self.node_connection_matrix[16][34]=1
        self.node_connection_matrix[17][30]=1
        self.node_connection_matrix[21][27]=1
        self.node_connection_matrix[22][26]=1
        self.node_connection_matrix[22][27]=1
        self.node_connection_matrix[22][23]=1
        self.node_connection_matrix[23][29]=1
        self.node_connection_matrix[24][25]=1
        self.node_connection_matrix[24][34]=1
        self.node_connection_matrix[28][27]=1
        self.node_connection_matrix[28][29]=1
        self.node_connection_matrix[30][39]=1
        self.node_connection_matrix[32][34]=1
        self.node_connection_matrix[33][34]=1
        self.node_connection_matrix[35][36]=1
        self.node_connection_matrix[36][37]=1
        self.node_connection_matrix[38][39]=1
        for i in range(0,40):
            for j in range(0,40):
                if self.node_connection_matrix[i][j]==1:
                    self.node_connection_matrix[j][i]=1

    @inheritdoc(Strategy)
    def process_event(self, time, receiver, content, log):
        #file naming
        """alpha=self.alpha
        name_alpha = str(alpha)
	cache_size = 1000 * self.network_cache
	name_cache = str(cache_size)
	name_file1=file_name("cl4m_served_by_nodes_server_up",name_alpha,name_cache)"""
	#name_file3=file_name("cl4m_content_population",name_alpha,name_cache)
	#get all required data
        n=100000
        source = self.view.content_source(content)
        path = self.view.shortest_path(receiver, source)
        # Route requests to original source and queries caches on the path
        self.controller.start_session(time, receiver, content, log)
        if self.count%100000==0:
            print "count: ",self.count
        if self.count<=2000000:
            for u, v in path_links(path):
                self.controller.forward_request_hop(u, v)
                if self.view.has_cache(v):
                    if self.controller.get_content(v):
                        serving_node = v
                        break
            # No cache hits, get content from source
            else:
                self.controller.get_content(v)
                serving_node = v
            # Return content
            path = list(reversed(self.view.shortest_path(receiver, serving_node)))
            # get the cache with maximum betweenness centrality
            # if there are more than one cache with max betw then pick the one
            # closer to the receiver
            max_betw = -1
            designated_cache = None
            for v in path[1:]:
                if self.view.has_cache(v):
                    if self.betw[v] >= max_betw:
                        max_betw = self.betw[v]
                        designated_cache = v
            # Forward content
            for u, v in path_links(path):
                self.controller.forward_content_hop(u, v)
                if v == designated_cache:
                    self.controller.put_content(v)
        else:
            #fo1.write('\ncount is: %d\n'%self.count)
            #fo1.write('source of content is: %d\n'%source)
            if self.count==2000001:
                #fo1.write('finalizing caches..')
                for content in range(1,n+1):
                    #fo1.write('content id is: %d\n'%content)
                    #print "content is: ",content
                    caches=[]
                    caches=self.view.content_locations(content)
                    #fo1.write('sources are: %s'%caches)
                    #print "caches for this content are: ",caches
                    for cache in caches:
                        if int(cache)<1000:
                            #print "cache is: ",cache
                            #print "content is: ",content
                            self.node_content_presence_table[cache][content-1][0]=1
                            #print "content added to this cache..\n\n"

            #fo1.write('\n\n\n\n')
            hopCount=0
            nextNode=receiver
            serving_node= source
            #sFlag= False
            #rFlag= False
            while hopCount<6:
                hopCount=hopCount+1
                #print "entering in hopcount.."
                #print "next node is: ",nextNode
                #fo1.write('searching in node: %d\n'%nextNode)
                #print "content id is: ",content
                #print "hop count is: ",hopCount
                #fo1.write('hop count is: %d\n'%hopCount)
                #print "element in node content presence table is: "
                #print self.node_content_presence_table[nextNode][content][0]
                if self.node_content_presence_table[nextNode][content-1][0]==1:
                    #serve content from this node, write code
                    #print "entered in first block.."
                    serving_node=nextNode
                    #if self.count>149000:
                        #fo1.write('1st block:serving node of content is: %d\n'%serving_node)
                    break
                elif self.node_breadcrumb_table[nextNode][content-1][0]>-1:
                    #forward request to this node
                    #update breadcrumb table
                    #print "entered in second block.."
                    nNode= self.node_breadcrumb_table[nextNode][content-1][0]
                    #sFlag= True
                    #if self.count>149000:
                        #fo1.write('2nd block:content entry found from breadcrumb table. next node is: %d\n'%nNode)
                    nextNode=nNode
                else:
                    #choose one router randomly from the set of routers with which the receiver is directly connected
                    #print "entered in last block.."
                    #nextNode=chooseRandNode(self.node_connection_matrix, receiver)
                    #fo1.write('content not found in cache or breadcrumb table\n')
                    conSet=[]
                    #rFlag= True
                    matLen=len(self.node_connection_matrix[0])
                    #print "length is: ",matLen
                    for i in range(0,40):
                        if self.node_connection_matrix[nextNode][i]==1:
                            conSet.append(i)
                            #print "appended i value is: ",i
                    conSetLen=len(conSet)
                    #print "conSetLen is: ",conSetLen
                    if conSetLen==0:
                        #print "no element in conSet.."
                        nextNode=2000
                    elif conSetLen==1:
                        #print "only 1 element in conset. that is: ",conSet[0]
                        nextNode=conSet[0]
                    else:
                        #print "more than 1 element in conSet.."
                        r=random.randint(1,conSetLen)
                        nextNode=conSet[r-1]
                    #if self.count>149000:
                        #fo1.write('3rd block:next node has been chosen randomly. hop count is: %d\n'%hopCount)
                    #print "next node has been chosen randomly. next node is: ",nextNode
                    if nextNode==2000:
                        #fo1.write('next node is 2000. terminating\n')
                        break
            #print "hopcount section over.."
            #fo1.write('hop count section over...\n\n\n')
            # Return content
            path = list(reversed(self.view.shortest_path(receiver, serving_node)))
            #calculation of request serving potential
            if self.count>2000000:
                caches=[]
                caches=self.view.content_locations(content)
                for cache in caches:
                    if int(cache)<1000:
                        self.s_by_potential+=1
                        break
            #path1=path.reverse()
            #print "content serving path is: ",path
            #print "serving node is: ",serving_node
            #print "requester is: ",receiver
            if serving_node<1000:
                self.s_by_cache+=1
                pathLen= len(path)
                if pathLen>=2:
                    #write code to update breadcrumb table
                    i=0
                    j=1
                    while j<pathLen:
                        if path[i]<1000 and path[j]<1000:
                            self.node_breadcrumb_table[path[j]][content-1][0]=path[i]
                            #print "updating breadcrumb table.."
                            #print "for %d"%path[j]
                            #print " value is %d"%path[i]
                        i+=1
                        j+=1
                #if self.count>100000:
                    #self.s_by_cache+=1
        #fo1= open('%s.txt'%name_file1,'a')
        #fo1.write('%d\n'%self.s_by_cache)
        #fo1.close()
        #fo1.write('breadcrumb history:\n\n')
        #if self.count>149000:
            #if sFlag==True and serving_node<1000:
                #self.s_by_breadcrumb+=1
            #if rFlag==True and serving_node<1000:
                #self.s_by_random+=1
        #sFlag= False
        #rFlag= False
        if self.count>2000000:
            if self.count%100000==0:
                fo= open('my_algo_cl4m_alpha_10.txt','a')
                fo.write('served_my algo: %d'%self.s_by_cache)
                fo.write('    potential: %d\n'%self.s_by_potential)
                fo.close()
                self.s_by_cache=0
                self.s_by_potential=0
            #fo1.write('\n\n\n\nserved by breadcrumb value is: %d\n'%self.s_by_breadcrumb)
            #fo1.write('served by random value is: %d\n'%self.s_by_random)
            #fo1.write('total requests service potential: %d\n'%self.s_by_potential)
            #fo1.write('total requests served by caches: %d'%self.s_by_cache)
        self.count+=1
        #fo1.close()
        self.controller.end_session()



@register_strategy('NRR')
class NearestReplicaRouting(Strategy):
    """Ideal Nearest Replica Routing (NRR) strategy.

    In this strategy, a request is forwarded to the topologically close node
    holding a copy of the requested item. This strategy is ideal, as it is
    implemented assuming that each node knows the nearest replica of a content
    without any signalling

    On the return path, content can be caching according to a variety of
    metacaching policies. LCE and LCD are currently supported.
    """

    @inheritdoc(Strategy)
    def __init__(self, view, controller, metacaching, **kwargs):
        super(NearestReplicaRouting, self).__init__(view, controller)
        if metacaching not in ('LCE', 'LCD'):
            raise ValueError("Metacaching policy %s not supported" % metacaching)
        self.metacaching = metacaching

    @inheritdoc(Strategy)
    def process_event(self, time, receiver, content, log):
        # get all required data
        locations = self.view.content_locations(content)
        nearest_replica = min(locations,
                              key=lambda s: sum(self.view.shortest_path(receiver, s)))
        # Route request to nearest replica
        self.controller.start_session(time, receiver, content, log)
        self.controller.forward_request_path(receiver, nearest_replica)
        self.controller.get_content(nearest_replica)
        # Now we need to return packet and we have options
        path = list(reversed(self.view.shortest_path(receiver, nearest_replica)))
        if self.metacaching == 'LCE':
            for u, v in path_links(path):
                self.controller.forward_content_hop(u, v)
                if self.view.has_cache(v) and not self.view.cache_lookup(v, content):
                    self.controller.put_content(v)
        elif self.metacaching == 'LCD':
            copied = False
            for u, v in path_links(path):
                self.controller.forward_content_hop(u, v)
                if not copied and v != receiver and self.view.has_cache(v):
                    self.controller.put_content(v)
                    copied = True
        else:
            raise ValueError('Metacaching policy %s not supported'
                             % self.metacaching)
        self.controller.end_session()


@register_strategy('RAND_BERNOULLI')
class RandomBernoulli(Strategy):
    """Bernoulli random cache insertion.

    In this strategy, a content is randomly inserted in a cache on the path
    from serving node to receiver with probability *p*.
    """

    @inheritdoc(Strategy)
    def __init__(self, view, controller, p=0.2, **kwargs):
        super(RandomBernoulli, self).__init__(view, controller)
        self.p = p
        self.alpha = kwargs['alpha']
	self.network_cache = kwargs['network_cache']
	self.count=1
	self.s_by_cache=0

    @inheritdoc(Strategy)
    def process_event(self, time, receiver, content, log):
        #file naming
        alpha=self.alpha
        name_alpha = str(alpha)
	cache_size = 1000 * self.network_cache
	name_cache = str(cache_size)
	name_file1=file_name("rand_bernoulli_served_by_nodes_server_up",name_alpha,name_cache)
        # get all required data
        source = self.view.content_source(content)
        path = self.view.shortest_path(receiver, source)
        # Route requests to original source and queries caches on the path
        self.controller.start_session(time, receiver, content, log)
        for u, v in path_links(path):
            self.controller.forward_request_hop(u, v)
            if self.view.has_cache(v):
                if self.controller.get_content(v):
                    serving_node = v
                    break
        else:
            # No cache hits, get content from source
            self.controller.get_content(v)
            serving_node = v
        if self.count>500000:
            if serving_node<1000:
                self.s_by_cache+=1
        if self.count==600000:
            fo1= open('%s.txt'%name_file1,'a')
            fo1.write('%d\n'%self.s_by_cache)
            fo1.close()
        # Return content
        path =  list(reversed(self.view.shortest_path(receiver, serving_node)))
        for u, v in path_links(path):
            self.controller.forward_content_hop(u, v)
            if v != receiver and self.view.has_cache(v):
                if random.random() < self.p:
                    self.controller.put_content(v)
        self.count+=1
        self.controller.end_session()

@register_strategy('RAND_CHOICE')
class RandomChoice(Strategy):
    """Random choice strategy

    This strategy stores the served content exactly in one single cache on the
    path from serving node to receiver selected randomly.
    """

    @inheritdoc(Strategy)
    def __init__(self, view, controller, **kwargs):
        super(RandomChoice, self).__init__(view, controller)
        self.alpha = kwargs['alpha']
	self.network_cache = kwargs['network_cache']
	self.count=1
	self.s_by_cache=0

    @inheritdoc(Strategy)
    def process_event(self, time, receiver, content, log):
        #file naming
        alpha=self.alpha
        name_alpha = str(alpha)
	cache_size = 1000 * self.network_cache
	name_cache = str(cache_size)
	name_file1=file_name("rand_choice_served_by_nodes_server_up",name_alpha,name_cache)
        # get all required data
        source = self.view.content_source(content)
        path = self.view.shortest_path(receiver, source)
        # Route requests to original source and queries caches on the path
        self.controller.start_session(time, receiver, content, log)
        for u, v in path_links(path):
            self.controller.forward_request_hop(u, v)
            if self.view.has_cache(v):
                if self.controller.get_content(v):
                    serving_node = v
                    break
        else:
            # No cache hits, get content from source
            self.controller.get_content(v)
            serving_node = v
        if self.count>500000:
            if serving_node<1000:
                self.s_by_cache+=1
        if self.count==600000:
            fo1= open('%s.txt'%name_file1,'a')
            fo1.write('%d\n'%self.s_by_cache)
            fo1.close()
        # Return content
        path =  list(reversed(self.view.shortest_path(receiver, serving_node)))
        caches = [v for v in path[1:-1] if self.view.has_cache(v)]
        designated_cache = random.choice(caches) if len(caches) > 0 else None
        for u, v in path_links(path):
            self.controller.forward_content_hop(u, v)
            if v == designated_cache:
                self.controller.put_content(v)
        self.count+=1
        self.controller.end_session()
