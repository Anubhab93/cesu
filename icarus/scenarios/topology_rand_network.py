import networkx as nx
import numpy as np
import fnss
import random as rnd
from collections import Counter

#the code has been written for a network consisting of 1 server, 24 routers and 15 receivers
#values can be changed or adjusted if needed
N_total=40
nodes=25
G=nx.DiGraph()
G.add_nodes_from(range(0,N_total))

arr = [[0 for x in range(nodes)] for y in range(nodes)]
#print "dimensions of arr is:\n"
#print np.shape(arr)

for i in range(0,nodes):
    for j in range(0,nodes):
        if i==j:
            arr[i][j]=1
        j+=1
    i+=1

for i in range(0,nodes):
    for j in range(i+1,nodes):
        k=rnd.random()
        if k>0.5:
            arr[i][j]=1
        else:
            arr[i][j]=0
        arr[j][i]=arr[i][j]
        j+=1
    i+=1

#print arr

#for i in range(0,nodes):
#    for j in range(0,nodes):
#        if i!=j:
#            if arr[i][j]==1:
#                G.add_edge(i,j)
#        j+=1
#    i+=1

connections=[]
for i in range(0,nodes):
    l=arr[i]
    t=l.count(1)
    connections.append(t)

#print "maximum value is: ", max(connections)
source=connections.index(max(connections))
print "source no is: ", source

routers = []
for i in range(0,nodes):
    if i!=source:
        routers.append(i)

print "routers are: ",routers

receivers=[]
for i in range(nodes,N_total):
    receivers.append(i)

print "receivers are: ",receivers

for i in range(0,nodes):
    for j in range(0,nodes):
        if i!=j:
            if arr[i][j]==1:
                G.add_edge(i,j)
        j+=1
    i+=1

for rcvr in receivers:
    k=rnd.choice(routers)
    G.add_edge(rcvr,k)


nx.write_graphml(G,"rand_network.graphml")

