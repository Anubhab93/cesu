from __future__ import division

from os import path

import networkx as nx
import fnss
import random
from heapq import nlargest

topology = fnss.topologies.randmodels.erdos_renyi_topology(80, 0.4, seed=None, fast=False)
deg = nx.degree(topology)
all_nodes = topology.nodes()
#defining a receiver
no_receivers=20
try:
    receiver_attachments = random.sample(range(1, 80), no_receivers)   #we choose some nodes randomly and attach receivers to them
except ValueError:
    print('Error choosing receivers..')
receivers=[]
for v in receiver_attachments:
    u = v + 500
    topology.add_edge(v, u)
    receivers.append(u)
#defining content custodians
custodians = 10    # no. of custodians, we can choose any value
deg_nodes=[]        
for i in range (0, len(all_nodes)):
    #stores degree of every node in the list
    deg_nodes.append(topology.degree(i))
#choose 'custodians' number of nodes which have highest degrees among the nodes and
#store the values of the degrees in the list dummy_sources
dummy_sources = nlargest ( custodians, deg_nodes)
dummy_sources = set(dummy_sources)
print "highest degrees are: ",dummy_sources
sources=[]
for v in dummy_sources:
    #now we find corresponding node IDs
    k = [i for i, x in enumerate(deg_nodes) if x == v]
    """to remove duplicates we used the previous line. for example if there are 3 nodes with same degree,
    we always get the node ID of the first one using index method, we don't get that of other 2"""
    for j in k:
        sources.append(j)
if len(sources) > custodians:
    sources = sources [0 : custodians]
routers = [v for v in topology.nodes() if v not in sources + receivers]
print "sources: ",sources
print "routers: ",routers
print "receivers: ",receivers
