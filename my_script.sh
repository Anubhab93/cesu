#!/bin/sh

# Enable command echo
set -v

# Directory where this script is located
CURR_DIR=`pwd`

# Icarus main folder
ICARUS_DIR=${CURR_DIR}/../..

# Config file
CONFIG_FILE=${CURR_DIR}/config.py

# FIle where results will be saved
RESULTS_FILE=${CURR_DIR}/results.pickle

# Add Icarus code to PYTHONPATH
export PYTHONPATH=${ICARUS_DIR}:$PYTHONPATH

COUNTER=0
while [  $COUNTER -lt 15 ]; do
             echo "Experiment Number is $COUNTER"
             python ${ICARUS_DIR}/icarus.py --results ${RESULTS_FILE} ${CONFIG_FILE}
             let COUNTER=COUNTER+1 
done

#mv D:\anubhab.txt C:\Users\Anubhab\Downloads
#echo File Moved