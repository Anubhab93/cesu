import networkx as nx
import numpy as np
import fnss
import csv
#G=nx.read_graphml('C:\Users\Bitan\Dropbox\ICN New work\Geant2012.graphml', unicode)
#nx.topological_sort(G,nbunch=None,reverse=False)
contents = 500
cache = 20
nodes = 12
alpha = 0.8
G=nx.DiGraph()
G.add_nodes_from(range(0,nodes-1))
G.add_edge(0,6)
G.add_edge(1,6)
G.add_edge(6,8)
G.add_edge(2,8)
G.add_edge(3,7)
G.add_edge(4,7)
G.add_edge(7,9)
G.add_edge(5,9)
G.add_edge(9,10)
G.add_edge(8,10)
G.add_edge(10,11)
#write graph
nx.write_graphml(G,"test.graphml")
#topological sort
a = nx.topological_sort(G,nbunch=None,reverse=False)
receiver = []
#get the receivers
for i in range(0,nodes-1):
    if G.degree(i) == 1:
        receiver.append(i)
custodian = 11
excd = []
excd = receiver
excd.append(custodian)
#create the matrix for incoming links
b = [[0 for x in range(nodes)] for x in range(nodes)]
for i in range(0,nodes-1):
    if i is not custodian:
	if nx.has_path(G,source=i,target=custodian):
            path = nx.shortest_path(G,source=i,target=custodian)
	    if len(path) > 0:
	        j = path[1]
                b[j][i] = 1        
#create the matrix for content flow and caching matrix with all zeros
c = [[0 for x in range(contents)] for x in range(nodes)]
d = [[0 for x in range(contents)] for x in range(nodes)]
cache_mat = [[0 for x in range(contents)] for x in range(nodes)]
pdf = np.arange(1.0, contents+1.0)**-alpha
pdf /= np.sum(pdf)
for i in range(0,nodes-1):
    if i in receiver:
            c[i][:] = pdf
#update the caching matrix
for i in range (0,len(a)-1):
    #parse according to DAG
    x1 = a[i]
    nodelist = b[x1][:]
    #verify it is not receiver or custodian
    if x1 not in excd:
        nei = [j for j in range(len(nodelist)) if nodelist[j] == 1]
        #if there is any incoming link
        if len(nei) > 0:
            neic = [[0 for x in range(contents)] for x in range(nodes)]
            for k in range(0,len(nei)):
                nei_node = nei[k]
                neic[k][:] = c[nei_node][:]
            #sum up all the flows
            d[x1]=[sum(z) for z in zip(*neic)]
            temp = d[x1]
            temp1 = [0 for x in range(contents)]
            #sort the top flows
            indexes = sorted(range(len(temp)), key=lambda x: temp[x])[-cache:]
	    #print indexes
            for index in range(0,len(indexes)):
                temp[indexes[index]] = 0
                temp1[indexes[index]] = 1
            c[x1] = temp
            cache_mat[x1] = temp1
np.savetxt('mat.txt',cache_mat)
with open("mat.csv","wb") as f:
    writer = csv.writer(f)
    writer.writerows(cache_mat)

delay = []
data = np.loadtxt('log.txt', skiprows=540000)
data1 = data.astype(int)
for i in range(0, len(data1)-1):
    recv = data1[i,0]
    cont = data1[i,1]
    path = nx.shortest_path(G,source=recv,target=custodian)
    #print path
    for j in range(1, len(path)-2):
	cache = path[j]
	cs = cache_mat[cache]
	c1 = cs[cont-1]
	if c1 > 0:
	    d1 = j*4
	    delay.append(d1)
	    break
	else:
	    d1 = ((len(path)-2)*4+34)
	    delay.append(d1) 
latency = sum(delay)/len(data)
print latency
